//
//  RNDHeightProtocol.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 05.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

protocol RNDHeightProtocol: class {
    var height: CGFloat? { get set }
    var handlerUpdateHeight: EmptyBlock? { get set }
}
