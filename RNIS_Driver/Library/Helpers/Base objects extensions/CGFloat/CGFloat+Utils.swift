//
//  CGFloat+Utils.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 14.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension CGFloat {
    var stringComma: String {
        return (self as NSNumber).stringComma
    }
    
    var stringCommaTwoFraction: String {
        return (self as NSNumber).stringCommaTwoFraction
    }
    
    func evolutePercent(_ isPercentValue: Bool?, round: Bool = true) -> String? {
        guard let isPercentValue = isPercentValue else {
            return nil
        }
        var atil = self
        var lastSuffix = ""
        if isPercentValue {
            atil *= CGFloat(100)
            lastSuffix = "%"
        }
        if round {
            atil = CGFloat(Int(atil))
        }
        return atil.stringCommaTwoFraction + lastSuffix
    }
}
