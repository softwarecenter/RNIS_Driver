//
//  UIViewController+Utils.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 28.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension UIViewController {
    var asImage: UIImage? {
        return view.asImage
    }
}
