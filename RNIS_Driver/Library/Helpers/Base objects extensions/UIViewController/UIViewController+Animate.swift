//
//  UIViewController+Animate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 27.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension UIViewController {
    
    func preparePullAnimation(_ image: UIImage?) {
        guard let image = image else {
                return
        }
        let duration: CFTimeInterval = 0.3
        let imageView = UIImageView(image: image)
        imageView.frame = UIScreen.bounds()
        STRouter.window??.addSubview(imageView)
        imageView.animateRightPullHide(duration)
        view.blackViewAnimate(duration)
    }
    

    func preparePushAnimation(_ oldVC: UIViewController?) {
        guard let vc = oldVC,
            let image = vc.view.asImage else {
            return
        }
        let duration: CFTimeInterval = 0.3
        let imageView  = preparaAnimateOldImage(image, duration: duration)
        let transition = CATransition()
        transition.duration = duration
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.completion = { value in
            imageView.removeFromSuperview()
        }
        view.layer.add(transition, forKey: "animationPush")
    }
    
    fileprivate  func preparaAnimateOldImage(_ image: UIImage, duration: CFTimeInterval = 0.5) -> UIImageView {
        let imageView = UIImageView(image: image)
        imageView.frame = UIScreen.bounds()
        STRouter.window??.insertSubview(imageView, at: 0)
        imageView.blackViewAnimate(duration)
        return imageView
    }
}
