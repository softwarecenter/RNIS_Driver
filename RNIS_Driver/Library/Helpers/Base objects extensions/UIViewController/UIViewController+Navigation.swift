//
//  UIViewController+Navigation.swift
//  RNIS
//
//  Created by Артем Кулагин on 04.08.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import Foundation

extension UIViewController {
    
    func pushAnimated(completion: EmptyBlock? = nil) {
        STRouter.pushAnimated(self, completion: completion)
    }

    func pushAnimated() {
        STRouter.pushAnimated(self)
    }
    
    func push(_ animated: Bool = false,
              completion: EmptyBlock? = nil) {
        STRouter.push(self, animated: animated, completion: completion)
    }
    
    var board: UIViewController? {
        return STRouter.boardContainer(self)
    }
}
