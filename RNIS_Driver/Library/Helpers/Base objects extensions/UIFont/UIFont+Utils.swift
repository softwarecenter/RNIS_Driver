//
//  UIFont+AppDefaultFonts.swift
//  Spytricks
//
//  Created by Ivan Alekseev on 13.09.16.
//  Copyright © 2016 Ivan Alekseev. All rights reserved.
//

import UIKit

extension UIFont {
    
    static var cffazm8:UIFont {
        return cffazm(8)
    }
    
    static var cffazm12:UIFont {
        return cffazm(12)
    }
    
    static var cffazm16:UIFont {
        return cffazm(16)
    }
    
    static var cffazm18:UIFont {
        return cffazm(18)
    }
    
    static var cffazm20:UIFont {
        return cffazm(20)
    }
    
    static var cffazm22:UIFont {
        return cffazm(22)
    }
    
    static var cffazm24:UIFont {
        return cffazm(24)
    }
    
    static var cffazm26:UIFont {
        return cffazm(26)
    }
    
    static func cffazm(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Cffazmiakhaddpiofffvylaqekz", size: size)!
    }
    
    static var robotoRegular12: UIFont {
        return robotoRegular(12)
    }
    
    static var robotoRegular13: UIFont {
        return robotoRegular(13)
    }
    
    static var robotoRegular14: UIFont {
        return robotoRegular(14)
    }
    
    static var robotoMedium16: UIFont {
        return robotoMedium(16)
    }
    
    static var robotoMedium18: UIFont {
        return robotoMedium(18)
    }
    
    static func robotoRegular(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Regular", size: size) ?? UIFont()
    }
    
    static func robotoMedium(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Medium", size: size) ?? UIFont()
    }
}
