//
//  UIColor+Utils.swift
//  RNIS
//
//  Created by Артем Кулагин on 25.07.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import Foundation

import UIKit

extension UIColor {
    
    static var backColor: UIColor {
        return .F1645A
    }
    
    static var color13: UIColor {
        return #colorLiteral(red: 0.05098039216, green: 0.568627451, blue: 0.8196078431, alpha: 1)
    }
    
    static var color34: UIColor {
        return #colorLiteral(red: 0.1333333333, green: 0.6980392157, blue: 0.9725490196, alpha: 1)
    }
    
    static var color125: UIColor {
        return #colorLiteral(red: 0.4901960784, green: 0.4901960784, blue: 0.4901960784, alpha: 0.7)
    }
    
    static var color163: UIColor {
        return #colorLiteral(red: 0.6392156863, green: 0.6352941176, blue: 0.5843137255, alpha: 1)
    }
    
    static var color234: UIColor {
        return #colorLiteral(red: 0.9176470588, green: 0.2823529412, blue: 0.2705882353, alpha: 1)
    }
    
    static var color241: UIColor {
        return #colorLiteral(red: 0.9450980392, green: 0.3921568627, blue: 0.3529411765, alpha: 1)
    }
    
    static var whiteAlpha50: UIColor {
        return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)
    }
    
    static var color01B3FD:UIColor {
        return #colorLiteral(red: 0.003921568627, green: 0.7019607843, blue: 0.9921568627, alpha: 1)
    }
    
    static var color4F4F4F:UIColor {
        return #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
    }
    
    static var A3423C:UIColor {
        return #colorLiteral(red: 0.6392156863, green: 0.2588235294, blue: 0.2352941176, alpha: 1)
    }
    
    static var AFAFAF:UIColor {
        return #colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1)
    }
    
    static var E25E55:UIColor {
        return #colorLiteral(red: 0.8862745098, green: 0.368627451, blue: 0.3333333333, alpha: 1)
    }
    
    static var EA4845:UIColor {
        return #colorLiteral(red: 0.9176470588, green: 0.2823529412, blue: 0.2705882353, alpha: 1)
    }
    
    static var D95A53:UIColor {
        return #colorLiteral(red: 0.8509803922, green: 0.3529411765, blue: 0.3254901961, alpha: 1)
    }
    
    static var F1645A:UIColor {
        return #colorLiteral(red: 0.9450980392, green: 0.3921568627, blue: 0.3529411765, alpha: 1)
    }
    
    static var FFB9AF:UIColor {
        return #colorLiteral(red: 1, green: 0.7254901961, blue: 0.6862745098, alpha: 1)
    }
    
    static var FC5C53:UIColor {
        return #colorLiteral(red: 0.9882352941, green: 0.3607843137, blue: 0.3254901961, alpha: 1)
    }
    
    static var F2F2F2:UIColor {
        return #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
    }
    
    static var F05558:UIColor {
        return #colorLiteral(red: 0.9411764706, green: 0.3333333333, blue: 0.3450980392, alpha: 1)
    }
    
    static var BB6BD9:UIColor {
        return #colorLiteral(red: 0.7333333333, green: 0.4196078431, blue: 0.8509803922, alpha: 1)
    }
    
    static var F2994A:UIColor {
        return #colorLiteral(red: 0.9490196078, green: 0.6, blue: 0.2901960784, alpha: 1)
    }
    
    static var color6FCF97:UIColor {
        return #colorLiteral(red: 0.4352941176, green: 0.8117647059, blue: 0.5921568627, alpha: 1)
    }
    
    static var color56CCF2:UIColor {
        return #colorLiteral(red: 0.337254902, green: 0.8, blue: 0.9490196078, alpha: 1)
    }
    
    static var DE4597:UIColor {
        return #colorLiteral(red: 0.8705882353, green: 0.2705882353, blue: 0.5921568627, alpha: 1)
    }
    
    static var colorShadow: UIColor {
        return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.25)
    }
    
    static var blackAlpha50: UIColor {
        return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
    }
    
    static var graph: UIColor {
        return #colorLiteral(red: 0.462745098, green: 0.5568627451, blue: 0.6784313725, alpha: 1)
    }
    
    static var color9C9C9C: UIColor {
        return #colorLiteral(red: 0.6117647059, green: 0.6117647059, blue: 0.6117647059, alpha: 1)
    }
    
    static var BDBDBD: UIColor {
        return #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
    }
    
    static var E0E0E0: UIColor {
        return #colorLiteral(red: 0.8784313725, green: 0.8784313725, blue: 0.8784313725, alpha: 1)
    }
    
    static var F2C44C: UIColor {
        return #colorLiteral(red: 0.9490196078, green: 0.768627451, blue: 0.2980392157, alpha: 1)
    }
    
    static var color219653: UIColor {
        return #colorLiteral(red: 0.1294117647, green: 0.5882352941, blue: 0.3254901961, alpha: 1)
    }
    
    static var color57AF7D: UIColor {
        return #colorLiteral(red: 0.3411764706, green: 0.6862745098, blue: 0.4901960784, alpha: 1)
    }
    
    static var F4A44B: UIColor {
        return #colorLiteral(red: 0.9568627451, green: 0.6431372549, blue: 0.2941176471, alpha: 1)
    }
    
    static var F15659: UIColor {
        return #colorLiteral(red: 0.9450980392, green: 0.337254902, blue: 0.3490196078, alpha: 1)
    }
    
    static var color454B52: UIColor {
        return #colorLiteral(red: 0.2705882353, green: 0.2941176471, blue: 0.3215686275, alpha: 1)
    }
    
    
}
