//
//  UIView+Animate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 27.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension UIView {
    
    func animateRightPullHide(_ duration: CFTimeInterval = 0.5) {
        let width = UIScreen.width
        let rect = CGRect(x: width, y: frame.origin.y, width: width, height: frame.size.height)
        UIView.animate(withDuration: duration, animations: { [weak self] in
            self?.frame = rect
            }, completion: { [weak self] value in
                self?.removeFromSuperview()
        })
    }
    
    func blackViewAnimate(_ duration: CFTimeInterval = 0.5, show: Bool = true) {
        let start: CGFloat = 0
        let finish: CGFloat = 0.1
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = show ? start : finish
        view.frame = bounds
        addSubview(view)
        UIView.animate(withDuration: duration, animations: {
            view.alpha = show ? finish : start
        }) { [weak view] value in
            view?.removeFromSuperview()
        }
    }
}
