//
//  DateFormatter+Utils.swift
//  Spytricks
//
//  Created by Артем Кулагин on 17.03.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    static var textHHmm: String {
        return "HH:mm"
    }
    
    static var textDDMMyyyy: String {
        return "dd.MM.yyyy"
    }
    
    static var textDDMM: String {
        return "dd.MM"
    }
    
    static var textyyyy: String {
        return "yyyy"
    }
    
    static var textMMMMyyyy: String {
        return "MMMM yyyy"
    }
    
    static var textLLLLyyyy: String {
        return "LLLL yyyy"
    }
    
    static var HHmm: DateFormatter{
        return format(textHHmm)
    }
    
    static var DDMMyyyy: DateFormatter{
        return format(textDDMMyyyy)
    }
    
    static var DDMM: DateFormatter{
        return format(textDDMM)
    }
    
    static var DDMMyyyy_HHmm: DateFormatter{
        return format(textDDMMyyyy + " " + textHHmm)
    }
    
    static var MMMMyyyy: DateFormatter{
        return format(textMMMMyyyy)
    }
    
    static var yyyy: DateFormatter{
        return format(textyyyy)
    }
    
    static var E_d_MMMM: DateFormatter{
        return format("E d MMMM" + " / " + textHHmm)
    }
    
    static var LLLLyyyy: DateFormatter{
        return format(textLLLLyyyy)
    }
    
    static func format(_ format: String) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter
    }
}
