//
//  NSNumber+Utils.swift
//  Spytricks
//
//  Created by Артем Кулагин on 21.06.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import Foundation

extension NSNumber {
    
    var stringComma: String {
        return stringAt(.stringComma)
    }
    
    var stringCommaTwoFraction: String {
        return stringAt(.stringCommaTwoFraction)
    }
    
    func stringAt(_ formatter: NumberFormatter) -> String {
        let text = formatter.string(from: self)
        return text ?? String(describing: self)
    }
}
