//
//  Array.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 19.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension Array where Element: RNDElaboraField {
    var summKoef: CGFloat {
        var summKoef = CGFloat(0)
        for item in self {
            summKoef += item.koef
        }
        return summKoef
    }
}
