//
//  NumberFormatter+Utils.swift
//  Spytricks
//
//  Created by Артем Кулагин on 21.06.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import Foundation

extension NumberFormatter {
    
    static var stringComma: NumberFormatter {
        return stringComma(1)
    }
    
    static var stringCommaTwoFraction: NumberFormatter {
        return stringComma(2)
    }
    
    static func stringComma(_ maximumFractionDigits: Int) -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.decimalSeparator = "."
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = maximumFractionDigits
        formatter.minimumIntegerDigits = 1
        return formatter
    }
}
