//
//  UILabel+Utils.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 29.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension UILabel {
    
    var widthText: CGFloat {
        return text?.width(font) ?? 0
    }
}
