//
//  Date+Utils.swift
//  RNIS
//
//  Created by Артем Кулагин on 22.08.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import Foundation

extension Date {
    
    func appendMinute(_ value: Int?) -> Date? {
       return dateByAddingUnit(.minute, value: value ?? 0)
    }
 
    func appendMonth(_ value: Int?) -> Date? {
        return dateByAddingUnit(.month, value: value ?? 0)
    }
    
    var appendQuarter: Date? {
        return appendMonth(3)
    }
    
    var appendHalf_year: Date? {
        return appendMonth(6)
    }
    
    var appendYear: Date? {
        return dateByAddingUnit(.year, value: 1)
    }
    
    var minusSecond: Date? {
        return dateByMinusUnit(.second, value: 1)
    }
    
    var minusMonth: Date? {
        return appendMonth(-1)
    }
    
    var appendDay: Date? {
        return appendDay(1)
    }
    
    func appendDay(_ value: Int?) -> Date? {
        return dateByAddingUnit(.day, value: value ?? 0)
    }
    
    static var currentMinusMonth: Date? {
        return Date().minusMonth
    }
    
    func dateByMinusUnit(_ unit: Calendar.Component, value: Int) -> Date? {
        return dateByAddingUnit(unit, value: -value)
    }
    
    func dateByAddingUnit(_ unit: Calendar.Component, value: Int) -> Date? {
        return calender.date(byAdding: unit, value: value, to: self)
    }
    
    var calender: Calendar {
        return Calendar.current
    }
}
