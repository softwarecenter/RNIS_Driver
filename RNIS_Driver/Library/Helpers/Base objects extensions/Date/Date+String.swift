//
//  Date+String.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 30.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension Date {
    
    var stringHHmm: String {
        return DateFormatter.HHmm.string(from: self)
    }
    
    var stringE_d_MMMM: String {
        return DateFormatter.E_d_MMMM.string(from: self)
    }
    
    var stringDDMMyyyy_HHmm: String {
        return DateFormatter.DDMMyyyy_HHmm.string(from: self)
    }
    
    var stringDDMMyyyy: String {
        return DateFormatter.DDMMyyyy.string(from: self)
    }
    
    var stringDDMM: String {
        return DateFormatter.DDMM.string(from: self)
    }
    
    var stringMMMMyyyy: String {
        return DateFormatter.MMMMyyyy.string(from: self)
    }
    
    var stringyyyy: String {
        return DateFormatter.yyyy.string(from: self)
    }
    
    var stringLLLLyyyy: String {
        return DateFormatter.LLLLyyyy.string(from: self)
    }
    
    
}
