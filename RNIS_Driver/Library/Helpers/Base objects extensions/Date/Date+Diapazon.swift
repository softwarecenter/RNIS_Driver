//
//  Date+Diapazon.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 30.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension Date {
    
    var month: Int {
        return valueComponent(.month)
    }
    
    var week: Int {
        return valueComponent(.weekOfYear)
    }
    
    var quarter: Int {
        return roundMonth(3)
    }
    
    var half_year: Int {
        return roundMonth(6)
    }
    
    var year: Int {
        return  valueComponent(.year)
    }
    
    var stringWeek: String {
        return "\(week) неделя " + stringyyyy_g
    }

    var stringQuarter: String {
        return "\(quarter) квартал " + stringyyyy_g
    }
    
    var stringHalf_year: String {
        return "\(half_year) полугодие " + stringyyyy_g
    }
    
    var stringyyyy_g: String {
        return stringyyyy + yearTextShort
    }
    
    func roundMonth(_ value: Float) -> Int {
        return Int(ceil(Float(month)/Float(value)))
    }
    
    func valueComponent(_ component: Calendar.Component) -> Int {
        return calender.component(component , from: self)
    }
}
