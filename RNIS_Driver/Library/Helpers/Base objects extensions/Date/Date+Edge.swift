//
//  Date+LastFirst.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 04.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension Date {
    
    var edgeDateComponents: DateComponents {
        return calender.dateComponents([.year, .month, .weekOfMonth, .day, .hour, .minute, .second], from: self)
    }

    var startOfWeek : Date? {
        return dayOfWeek(1)
    }
    
    func dayOfWeek(_ weekday: Int?) -> Date? {
        guard let weekday = weekday else {
            return nil
        }
        var cal = Calendar.current
        var component = cal.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)
        component.to12am()
        cal.firstWeekday = weekday + 1
        return cal.date(from: component)
    }
    
    var endOfWeek: Date? {
        guard let dayOfWeek = dayOfWeek(7) else {
            return nil
        }
        let cal = Calendar.current
        var component = DateComponents()
        component.weekOfYear = 1
        component.day = -1
        component.to12pm()
        return cal.date(byAdding: component, to: dayOfWeek)
    }
    
    var startOfMonth: Date {
        var comp = self.edgeDateComponents
        comp.setValue(1, for: .day)
        comp.to12am()
        return calender.date(from: comp)!
    }
    
    var endOfMonth: Date {
        let dayRange = calender.range(of: .day, in: .month, for: self)
        let dayCount = dayRange!.count
        var comp = self.edgeDateComponents
        comp.day = dayCount
        comp.to12pm()
        return calender.date(from: comp)!
    }
    
    var startOfQuarter: Date? {
        let startOfMonth = Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
        
        var components = Calendar.current.dateComponents([.month, .day, .year], from: startOfMonth)
        guard let month = components.month else {
            return nil
        }
        let newMonth: Int
        switch month {
        case 1,2,3: newMonth = 1
        case 4,5,6: newMonth = 4
        case 7,8,9: newMonth = 7
        case 10,11,12: newMonth = 10
        default: newMonth = 1
        }
        components.month = newMonth
        return Calendar.current.date(from: components)!
    }
    
    var endOfQuarter: Date? {
        return startOfQuarter?.appendQuarter?.minusSecond
    }
    
    var startOfHalf_year: Date? {
        let startOfMonth = Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
        var components = Calendar.current.dateComponents([.month, .day, .year], from: startOfMonth)
        guard let month = components.month else {
            return nil
        }
        components.month = (month <= 6) ? 1 : 7
        return Calendar.current.date(from: components)!
    }
    
    var endOfHalf_year: Date? {
        return startOfHalf_year?.appendHalf_year?.minusSecond
    }
    
    var startOfYear: Date? {
        var components = Calendar.current.dateComponents([.year], from: self)
        guard let startDateOfYear = Calendar.current.date(from: components) else {
            return nil
        }
        components.year = 0
        components.day = 0
        return Calendar.current.date(byAdding: components, to: startDateOfYear)
    }
    
    var endOfYear: Date? {
        return startOfYear?.appendYear?.minusSecond
    }
}

