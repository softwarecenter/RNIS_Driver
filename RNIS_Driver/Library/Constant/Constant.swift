//
//  Constant.swift
//  RNIS
//
//  Created by Артем Кулагин on 18.07.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import CoreLocation
let redPlaceLocation = CLLocation(latitude: 55.754289, longitude: 37.619800) //Красная площадь
//var stubLocation: CLLocation?// = CLLocation(latitude: 59.9363783, longitude: 30.3022305) //питер
var stubLocation: CLLocation?// = redPlaceLocation// = CLLocation(latitude: 55.754289, longitude: 37.619800) //Красная площадь

let mapHost = "http://95.213.205.92/"
let mapHost2 = "http://95.213.205.91/"

let serverAddress = ""
//let serverRnisapi = "https://dev-rnisapi.regeora.ru/ajax/request"
let serverRnisapi = "https://api.rnis.mosreg.ru/ajax/request"
let showLogApi = false
let showLogApiDetail = false


let kPhoneVC = "RNSPhoneViewController"
let kCodeVC = "RNSCodeViewController"
let kParoleVC = "RNSParoleViewController"
let kUuid = "uuid"
let kStrelka = "Стрелка"
let kNews = "Новости"
let kUpdateTime = "updateTime"
let kUpdateLocation = "updateLocation"
let kServerNotAviable = "Сервер недоступен"
let kName = "name"
let kMeta = "meta"
let yearText = " год"
let yearTextShort = " г."

let kRating = "rating"
let kGlonass = "glonass"
let kAge = "age"
let kWork = "work"

let kRegular_voyage_points = "regular_voyage_points"
let kCard_arrow = "card_arrow"
let kDobrodel = "dobrodel"

let edgeGroup: CGFloat = 30
let heightNav: CGFloat = 65
let heightHeaderElabora: CGFloat = 66
