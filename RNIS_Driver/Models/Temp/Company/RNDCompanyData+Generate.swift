//
//  RNDCompanyData+Generate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDCompanyData {
    
    static func gen(_ name: String?, value: CGFloat?) -> RNDCompanyData {
        let item = RNDCompanyData()
        item.name = name
        item.value = value
        return item
    }
    
    static func gen(_ name: String?, rating: CGFloat?, color: UIColor?) -> RNDCompanyData {
        let item = RNDCompanyData()
        item.name = name
        item.regular_voyage_points = CGFloat.rand(0, limit: 10)
        item.card_arrow = CGFloat.rand(0, limit: 10)
        item.dobrodel = CGFloat.rand(0, limit: 10)
        item.age = CGFloat.rand(0, limit: 10)
        item.rating = CGFloat.rand(0, limit: 10)
        item.color_rating = color
        return item
    }
    
    
    static func genGreen(_ name: String?, rating: CGFloat?) -> RNDCompanyData {
        return gen(name, rating: rating, color: .color57AF7D)
    }
    
    static func genOrange(_ name: String?, rating: CGFloat?) -> RNDCompanyData {
        return gen(name, rating: rating, color: .F4A44B)
    }
    
    static func genRed(_ name: String?, rating: CGFloat?) -> RNDCompanyData {
        return gen(name, rating: rating, color: .F15659)
    }
}
