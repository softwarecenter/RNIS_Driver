//
//  RNDCompanyData.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDCompanyData: NSObject {
    var uid: String?
    var name: String?
    var value: CGFloat?
    var regular_voyage_points: CGFloat?
    var card_arrow: CGFloat?
    var dobrodel: CGFloat?
    var age: CGFloat?
    var rating: CGFloat?
    var color_rating: UIColor?
}
