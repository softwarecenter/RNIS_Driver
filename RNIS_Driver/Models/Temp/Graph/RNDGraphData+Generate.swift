//
//  RNDGraphData+Generate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 08.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDGraphData {
    
    static var generate: RNDGraphData {
        var items = [RNDGraphItem]()
        
        for _ in 0...Int.rand(7, limit: 20) {
            let item = RNDGraphItem.generate
            item.date = (items.last?.date?.appendDay ?? Date())
            items.append(item)
        }
        let item = RNDGraphData()
        item.items = items
        item.isPercent = Bool.random
        return item
    }
}
