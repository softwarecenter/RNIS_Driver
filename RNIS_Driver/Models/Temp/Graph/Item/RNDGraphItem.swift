//
//  RNDGraphItem.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 08.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDGraphItem: NSObject {
    var date: Date?
    var value: CGFloat?
    
    var valueUnwrup: CGFloat {
        return value ?? 0
    }
    
    var dateString: String? {
        return date?.stringDDMM
    }
}
