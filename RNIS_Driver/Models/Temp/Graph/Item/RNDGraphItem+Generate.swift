//
//  RNDGraphItem+Generate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 08.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDGraphItem {
    
    static var generate: RNDGraphItem  {
        let item = RNDGraphItem()
        item.value = CGFloat.rand(5, limit: 20)
        item.date = Date()
        return item
    }
}
