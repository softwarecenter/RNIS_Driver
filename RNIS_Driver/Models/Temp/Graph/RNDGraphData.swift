//
//  RNSGraphData.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 08.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDGraphData: NSObject {
    
    var isPercent: Bool?
    var items: [RNDGraphItem]?
    
    var maxValue: CGFloat {
        let itemMax = items?.max(by: {
            $0.valueUnwrup < $1.valueUnwrup
        })
        return itemMax?.value ?? 0
    }
}
