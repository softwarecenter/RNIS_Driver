//
//  RNDElaboraTable.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDElaboraTableData: NSObject {
    var fields: [RNDElaboraField]?
    var items: [RNDCompanyData]?
}
