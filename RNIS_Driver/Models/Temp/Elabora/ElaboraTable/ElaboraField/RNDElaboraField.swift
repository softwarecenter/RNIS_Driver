//
//  RNDElaboraField.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDElaboraField: NSObject {
    var key: String?
    var title: String?
    
    var keyValue: String {
        return key ?? ""
    }
    
    var koef: CGFloat {
        let defaultKoef: CGFloat = 1
        switch keyValue {
        case kName:
            return 2.04
        case kRegular_voyage_points:
            return 1.39
        case kCard_arrow:
            return 1.44
        case kDobrodel:
            return 1.2
        case kAge:
            return defaultKoef
        case kRating:
            return 1.41
        default:
            return defaultKoef
        }
    }
}
