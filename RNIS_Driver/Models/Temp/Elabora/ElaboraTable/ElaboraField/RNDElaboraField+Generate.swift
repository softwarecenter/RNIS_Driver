//
//  RNDElaboraField+Generate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDElaboraField {
    static func gen(_ key: String?, title: String?) -> RNDElaboraField {
        let item = RNDElaboraField()
        item.key = key
        item.title = title
        return item
    }
}
