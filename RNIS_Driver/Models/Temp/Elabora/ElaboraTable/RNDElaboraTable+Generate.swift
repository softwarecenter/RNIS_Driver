//
//  RNDElaboraTable+Generate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDElaboraTableData {
    static var generate: RNDElaboraTableData {
        let item = RNDElaboraTableData()
        item.genFields()
        item.genCompanys()
        return item
    }
    
    func genFields() {
        fields = [RNDElaboraField.gen(kName, title: "Перевозчик"),
                  RNDElaboraField.gen(kRegular_voyage_points, title: "Регулярность рейсов, баллы"),
                  RNDElaboraField.gen(kCard_arrow, title: "Карта “Стрелка”, баллы"),
                  RNDElaboraField.gen(kDobrodel, title: "Жалобы “Добродел"),
                  RNDElaboraField.gen(kAge, title: "Возраст ТС"),
                  RNDElaboraField.gen(kRating, title: "Рейтинг")]
    }
    
    func genCompanys() {
        items = [RNDCompanyData.genGreen("ООО “РегионАвто”", rating: 2.83),
                 RNDCompanyData.genGreen("ООО “Долгопрудненское АТП”", rating: 2.77),
                 RNDCompanyData.genGreen("ГУП МО “Мострансавто”", rating: 2.17),
                 RNDCompanyData.genGreen("МП “Химкиэлетротранс”", rating: 2.00),
                 RNDCompanyData.genOrange("ООО “Транспорт 1”", rating: 1.93),
                 RNDCompanyData.genOrange("ООО “Клаксон +”", rating: 1.83),
                 RNDCompanyData.genOrange("ООО “АВТОМИГ”", rating: 1.77),
                 RNDCompanyData.genOrange("ООО “Автолайн-Мытищи”", rating: 1.67),
                 RNDCompanyData.genOrange("ООО “Аврора”", rating: 1.67),
                 RNDCompanyData.genOrange("ООО “Компания Автолайн”", rating: 1.67),
                 RNDCompanyData.genOrange("ООО “Стаффтранс 1”", rating: 1.64),
                 RNDCompanyData.genOrange("ООО “Стаффтранс”", rating: 1.53),
                 RNDCompanyData.genOrange("ООО “Клаксон-1”", rating: 1.50),
                 RNDCompanyData.genOrange("ООО “Клаксон”", rating: 1.50),
                 RNDCompanyData.genOrange("ООО “Альфа-Мобил”", rating: 1.33),
                 RNDCompanyData.genOrange("ООО “Северный тракт”", rating: 1.33),
                 RNDCompanyData.genOrange("ООО “Красногорск-Авто”", rating: 1.33),
                 RNDCompanyData.genOrange("ООО “КрасАвто”", rating: 1.33),
                 RNDCompanyData.genOrange("ООО “Автомагистраль”", rating: 1.33),
                 RNDCompanyData.genRed("ООО “Стрела”", rating: 1.00),
                 RNDCompanyData.genRed("ООО “РАНД-ТРАНС”", rating: 1.00),
                 RNDCompanyData.genRed("ООО “Балашиха-экспресс”", rating: 1.00),
                 RNDCompanyData.genRed("ООО “Автороуд”", rating: 1.00),
                 RNDCompanyData.genRed("ООО “Автокольцо”", rating: 0.67)]
    }
}
