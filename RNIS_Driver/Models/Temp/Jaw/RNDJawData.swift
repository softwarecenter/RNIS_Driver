//
//  RNDJawData.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 06.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDJawData: NSObject {
    var open: Bool? = false
    var type: String?
    var color: UIColor?
    var title: String?
    var values: [RNDPlankData]?
    var handlerUpdateUI: EmptyBlock?
    
    convenience init(_ type: String?, open: Bool?, color: UIColor?, title: String?) {
        self.init()
        self.open = open
        self.type = type
        self.color = color
        self.title = title
    }
    
    var height: CGFloat {
        return openValue ? 227 : 141
    }
    
    var openValue: Bool {
        return RNDDashManager.editState||(open ?? false)
    }
    
    func openClosed() {
        open = !openValue
    }
    
    static var items: [RNDJawData] {
       return [item0,item1,item2,item3,item4,item5]
    }
    
    var image: UIImage? {
        guard let type = type else {
            return nil
        }
        
        switch type {
        case "passenger":
            return #imageLiteral(resourceName: "passengerImg")
        case "children":
            return #imageLiteral(resourceName: "childrenImg")
        case "communal":
            return #imageLiteral(resourceName: "communalImg")
        case "garbage":
            return #imageLiteral(resourceName: "garbageImg")
        case "gu_gath":
            return #imageLiteral(resourceName: "gu_gathImg")
        default:
            return nil
        }
    }
    
    var inDeveloping: Bool {
        return (title != nil)&&((values?.count ?? 0) == 0)
    }
    
    func updateUI() {
        handlerUpdateUI?()
    }
    
    func showIfNeed(_ itemCalendar: RNDCalendarData? = nil) {
        guard !inDeveloping, !RNDDashManager.editState  else {
            return
        }
        let vc = RNDFractionViewController.initialControllerType()
        vc?.item = self
        vc?.title = title
        vc?.itemCalendar = itemCalendar
        vc?.pushAnimated()
    }
}
