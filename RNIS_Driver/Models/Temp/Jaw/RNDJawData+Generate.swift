//
//  RNDJawData+Generate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 07.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDJawData {
    static var item0: RNDJawData {
        let item = RNDJawData("passenger", open: Bool.random, color: .F05558, title: "Пассажирские перевозки")
        item.values = [RNDPlankData.genRating(1.54, change: 0.01, isPercent: false),
                       RNDPlankData.genGlonass(0.65, change: 0.05, isPercent: true),
                       RNDPlankData.genAge(4, change: 1, isPercent: false),
                       RNDPlankData.genWork(0.72, change: -0.12, isPercent: true)
            
        ]
        return item
    }
    
    static var item1: RNDJawData {
        return RNDJawData("road", open: false, color: .BB6BD9, title: "Обслуживание дорог")
    }
    
    static var item2: RNDJawData {
        let item =  RNDJawData("children", open: Bool.random, color: .F2994A, title: "Перевозки детей")
        item.values = [RNDPlankData.genRating(1.36, change: -0.01, isPercent: false),
                       RNDPlankData.genGlonass(0.65, change: -0.05, isPercent: true),
                       RNDPlankData.genAge(4, change: 0.5, isPercent: false),
                       RNDPlankData.genWork(0.68, change: -0.19, isPercent: true)
            
        ]
        return item
    }
    
    static var item3: RNDJawData {
        let item =  RNDJawData("communal", open: Bool.random, color: .color6FCF97, title: "Коммунальная техника")
        item.values = [RNDPlankData.genRating(1.12, change: -0.12, isPercent: false),
                       RNDPlankData.genGlonass(0.95, change: 0.36, isPercent: true),
                       RNDPlankData.genAge(7, change: 2, isPercent: false),
                       RNDPlankData.genWork(0.31, change: 0.26, isPercent: true)
            
        ]
        return item
    }
    
    static var item4: RNDJawData {
        let item =  RNDJawData("garbage", open: Bool.random, color: .color56CCF2, title: "Вывоз мусора")
        item.values = [RNDPlankData.genRating( 5.12, change: 0.2, isPercent: false),
                       RNDPlankData.genGlonass(0.56, change: -0.25, isPercent: true),
                       RNDPlankData.genAge(1, change: -3, isPercent: false),
                       RNDPlankData.genWork(0.95, change: 0.13, isPercent: true)
            
        ]
        return item
    }
    
    static var item5: RNDJawData {
        let item = RNDJawData("gu_gath", open: Bool.random, color: .DE4597, title: "ГУ ГАТН")
        item.values = [RNDPlankData.genRating(9.12, change: 0.05, isPercent: false),
                       RNDPlankData.genGlonass(0.68, change: -0.13, isPercent: true),
                       RNDPlankData.genAge(9, change: 3, isPercent: false),
                       RNDPlankData.genWork(0.58, change: 0.15, isPercent: true)
            
        ]
        return item
    }
}
