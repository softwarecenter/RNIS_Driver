//
//  RNDPlankData+GenDetail.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDPlankData {
    
    func generateDetail() {
        guard let key = key else {
            return
        }
        switch key {
        case kRating:
            gen("Рейтинг", color: .F2C44C)
        case kGlonass:
            gen("Оснащённость ГЛОНАСС", color: .F2C44C)
        case kAge:
            gen("Средний возраст автопарка", color: .color219653)
        case kWork:
            gen("Выполнение транспортной работы", color: .F2C44C)
        default: break
            
        }
    }
    
    func gen(_ title: String?, color: UIColor?) {
        title_detail = title
        self.color = color
        genCompany()
    }
    
    func genCompany() {
        if isPercent ?? false {
            self.best = genCompaniesBestPercent
            self.worst = genCompaniesWorstPercent
        } else {
            self.best = genCompaniesBest
            self.worst = genCompaniesWorst
        }
    }
    
    var genCompaniesBest: [RNDCompanyData]? {
        return [RNDCompanyData.gen("ООО “РегионАвто”", value: 2.83),
                RNDCompanyData.gen("ООО “Долгопрудненское АТП”", value: 2.77),
                RNDCompanyData.gen("ГУП МО “Мострансавто”", value: 2.83)]
    }
    
    var genCompaniesWorst: [RNDCompanyData]? {
        return [RNDCompanyData.gen("ООО “Автокольцо”", value: 0.67),
                RNDCompanyData.gen("ООО “Автороуд”", value: 0.67),
                RNDCompanyData.gen("ООО “Балашиха-экспресс”", value: 1)]
    }
    
    var genCompaniesBestPercent: [RNDCompanyData]? {
        return [RNDCompanyData.gen("ООО “Пиксарт”", value: 0.78),
                RNDCompanyData.gen("ГУП МО “МОСТРАНСАВТО”", value: 0.77),
                RNDCompanyData.gen("ООО “РегионАвто”", value: 0.76)]
    }
    
    var genCompaniesWorstPercent: [RNDCompanyData]? {
        return [RNDCompanyData.gen("ООО “Автороут”", value: 0.12),
                RNDCompanyData.gen("ООО “Стрела”", value: 0.13),
                RNDCompanyData.gen("ООО “Клаксон +”", value: 0.14)]
    }
}
