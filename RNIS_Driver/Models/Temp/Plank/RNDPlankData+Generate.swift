//
//  RNDPlankData+Generate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDPlankData {
    static func genRating(_ value: CGFloat?, change: CGFloat?, isPercent: Bool?) -> RNDPlankData {
        return RNDPlankData(kRating, title: "Рейтинг", value: value, change: change, isPercent: isPercent)
    }
    
    static func genGlonass(_ value: CGFloat?, change: CGFloat?, isPercent: Bool?) -> RNDPlankData {
        return RNDPlankData(kGlonass, title: "Оснащённость ГЛОНАСС", value: value, change: change, isPercent: isPercent)
    }
    
    static func genAge(_ value: CGFloat?, change: CGFloat?, isPercent: Bool?) -> RNDPlankData {
        return RNDPlankData(kAge, title: "Возраст автопарка", value: value, change: change, isPercent: isPercent)
    }
    
    static func genWork(_ value: CGFloat?, change: CGFloat?, isPercent: Bool?) -> RNDPlankData {
        return RNDPlankData(kWork, title: "Транспортная работа", value: value, change: change, isPercent: isPercent)
    }
}
