//
//  RNDPlankData.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 07.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDPlankData: NSObject {
    var key: String?
    var title: String?
    var value: CGFloat?
    var change: CGFloat?
    var isPercent: Bool?
    
    var title_detail: String?
    var color: UIColor?
    var best: [RNDCompanyData]?
    var worst: [RNDCompanyData]?
    
    var title_meaning: String?
    var table: RNDElaboraTableData?
    
    convenience init(_ key: String?, title: String?, value: CGFloat?, change: CGFloat?, isPercent: Bool?) {
        self.init()
        self.key = key
        self.title = title
        self.value = value
        self.change = change
        self.isPercent = isPercent
    }
    
    var isPercentValue: Bool {
        return isPercent ?? false
    }
    
    var textValue: String? {
        return value?.evolutePercent(isPercent)
    }
    
    var textChange: String? {
        guard var change = self.change else {
            return nil
        }
        if change < 0 {
            change = -change
        }
        return change.evolutePercent(isPercent)
    }
}
