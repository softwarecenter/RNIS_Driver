//
//  RNDPlankData+GenElabora.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDPlankData {
    func genElabora() {
        title_meaning = genTitleMeaning
        table = RNDElaboraTableData.generate
    }
    
    var genTitleMeaning: String? {
        guard let key = key else {
            return nil
        }
        switch key {
        case kRating:
            return "Рейтинг по пассажирским перевозкам"
        case kGlonass:
            return "Оснащённость ГЛОНАСС по пассажирским перевозкам"
        case kAge:
            return "Средний возраст автопарка по пассажирским перевозкам"
        case kWork:
            return "Выполнение транспортной работы по пассажирским перевозкам"
        default:
            return nil
        }
    }
}
