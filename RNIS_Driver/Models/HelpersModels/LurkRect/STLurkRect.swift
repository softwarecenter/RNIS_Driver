//
//  STLurkRect.swift
//  Spytricks
//
//  Created by Артем Кулагин on 12.10.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import UIKit

struct STLurkRect {
   
    static func circle(_ rect: CGRect) -> STLurkRect {
        return STLurkRect(rect, cornerRadius: rect.width/2)
    }
    
    init(_ rect: CGRect?, cornerRadius: CGFloat? = 1) {
        self.rect = rect
        self.cornerRadius = cornerRadius
    }
    
    var rect: CGRect?
    var cornerRadius: CGFloat?
}
