//
//  RNDEdgeDate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 05.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

struct RNDEdgeDate {
    var date1: Date?
    var date2: Date?
    var text: String?
    
    init(_ date: Date?, type: DiapazonType = .year) {
        self.date1 = type.startPeriod(date)
        self.date2 = type.endPeriod(date)
        self.text = type.string(self.date2)
    }
}
