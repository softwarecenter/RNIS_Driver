//
//  RNDEdgeDate+Compare.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 05.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDEdgeDate {
    func contains(_ date: Date?) -> Bool {
        guard let date = date else {
            return false
        }
        return date1Compare(date)&&date2Compare(date)
    }
    
    func date1Compare(_ date: Date) -> Bool {
        guard let date1 = date1 else {
            return false
        }
        let compare = date1.compare(date)
        return (compare == .orderedSame)||(compare == .orderedAscending)
    }
    
    func date2Compare(_ date: Date) -> Bool {
        guard let date2 = date2 else {
            return false
        }
        let compare = date.compare(date2)
        return (compare == .orderedSame)||(compare == .orderedAscending)
    }
}
