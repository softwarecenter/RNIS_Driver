//
//  DiapazonType.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 30.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

enum DiapazonType: String {
    case
    select = "Диапазон дат",
    weak = "Неделя",
    month = "Месяц",
    quarter = "Квартал",
    half_year = "Полугодие",
    year = "Год",
    now = "На текущий момент"
    
    static let items: [DiapazonType] = [.select,.weak,.month,.quarter,.half_year,.year,.now]
    
    var component: (Calendar.Component, Int)? {
        switch self {
        case .weak:
            return (.weekday, 7)
        case .month:
            return (.month,1)
        case .quarter:
            return (.month,3)
        case .half_year:
            return (.month,6)
        case .year:
            return (.year,1)
        default:
            return nil
        }
    }

    func string(_ date: Date?) -> String? {
        guard let date = date else {
            return nil
        }
        switch self {
        case .weak:
            return date.stringWeek
        case .month:
            return date.stringLLLLyyyy + yearTextShort
        case .quarter:
            return date.stringQuarter
        case .half_year:
            return date.stringHalf_year
        case .year:
            return date.stringyyyy + yearText
        default:
            return nil
        }
    }
    
    func startPeriod(_ date: Date?) -> Date? {
        switch self {
        case .weak:
            return date?.startOfWeek
        case .month:
            return date?.startOfMonth
        case .quarter:
            return date?.startOfQuarter
        case .half_year:
            return date?.startOfHalf_year
        case .year:
            return date?.startOfYear
        default:
            return nil
        }
    }
    
    func endPeriod(_ date: Date?) -> Date? {
        switch self {
        case .weak:
            return date?.endOfWeek
        case .month:
            return date?.endOfMonth
        case .quarter:
            return date?.endOfQuarter
        case .half_year:
            return date?.endOfHalf_year
        case .year:
            return date?.endOfYear
        default:
            return nil
        }
    }
    
    func itemEdge(_ date: Date?) -> RNDEdgeDate? {
        return RNDEdgeDate(date, type: self)
    }
}
