//
//  RNDCalendarData.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 15.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

struct RNDCalendarData {
    
    var date1 = Date.currentMinusMonth?.startOfMonth
    var date2 = Date.currentMinusMonth?.endOfMonth
    var type: DiapazonType = .month
    
    init(_ type: DiapazonType, date1: Date?, date2: Date?) {
        self.type = type
        self.date1 = date1
        self.date2 = date2
    }
}
