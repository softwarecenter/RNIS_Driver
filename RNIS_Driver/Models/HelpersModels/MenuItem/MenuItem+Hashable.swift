//
//  MenuItem+Hashable.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 20.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension MenuItem: Hashable {
    var hashValue: Int {
        return title?.hashValue ?? 0
    }
    
    static func ==(lhs: MenuItem, rhs: MenuItem) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}
