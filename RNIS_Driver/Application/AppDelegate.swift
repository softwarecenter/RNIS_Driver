//
//  AppDelegate.swift
//  RNIS_Driver
//
//  Created by Admin on 02.11.17.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //RNSTimeManager()
        STRouter.prepareWindowColor()
        
        STRouter.showWebTemp()
        //STRouter.showLogin()
        return true
    }
}

