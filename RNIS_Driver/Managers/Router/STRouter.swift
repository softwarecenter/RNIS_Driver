//
//  STRouter.swift
//  Spytricks
//
//  Created by Артем Кулагин on 10.02.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import UIKit

class STRouter: NSObject {
    static let shared = STRouter()
    
    static var rootViewController = BaseNavigationController()
    
    static var window: UIWindow?? {
        return UIApplication.shared.delegate?.window
    }
    
    static func prepareRoot(_ vc: UIViewController?) {
        window??.rootViewController = vc
    }
    
    static var screenRootViewController: UIImage? {
        return window??.rootViewController?.asImage
    }
    
    static func prepareWindowColor() {
        window??.backgroundColor = .white
    }
    
    static var rootView: UIView? {
        return rootViewController.view
    }
    
    static var viewControllers: [UIViewController]? {
        return rootViewController.viewControllers
    }
    
    static func push(_ viewController: UIViewController?,
                             animated: Bool = false,
                           completion: EmptyBlock? = nil) {
        guard let viewController = viewController else {
            return
        }
        rootViewController.push(viewController, animated: animated, completion: completion)
    }
    
    static func pushAnimated(_ viewController: UIViewController?, animated: Bool = true, completion: EmptyBlock? = nil) {
        push(viewController, animated: animated, completion: completion)
    }
    
    static func pop(animated: Bool = true, completion: EmptyBlock? = nil) {
        rootViewController.pop(animated: animated, completion: completion)
    }
    
    static func popToRoot(_ animated: Bool = true, completion:EmptyBlock? = nil) {
        rootViewController.popToRoot(animated: animated, completion: completion)
    }
    
    static func popTo(_ vc: UIViewController?) {
        guard let vc = vc else {
            return
        }
        rootViewController.popToViewController(vc, animated: true)
    }
    
    static func popNoAnimate(completion: EmptyBlock? = nil) {
        pop(animated: false, completion: completion)
    }
    
    static func present(_ viewControllerToPresent:UIViewController?, animated: Bool = true, completion: EmptyBlock? = nil) {
        var vc:UIViewController = rootViewController
        guard let viewControllerToPresent = viewControllerToPresent  else {
            return
        }
       
        while let presented = vc.presentedViewController {
            vc = presented
        }
        viewControllerToPresent.modalPresentationStyle = .overCurrentContext
        vc.present(viewControllerToPresent, animated: animated, completion: completion)
    }
    
    static func presentAsPopover(_ vc: UIViewController?, view: UIView?) {
        guard let view = view else {
            return
        }
        let popover = vc?.popoverPresentationController
        popover?.sourceView = view
        popover?.sourceRect = view.bounds
        present(vc)
    }
    
    static func clearNav() {
        rootViewController.viewControllers.removeAll()
    }
}
