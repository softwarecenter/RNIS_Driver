//
//  STRouter+Navigation.swift
//  RNIS
//
//  Created by Артем Кулагин on 21.07.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit

extension STRouter {
    
    static var navTitles: [String]? {
        return viewControllers?.flatMap{
            return $0.title ?? ""
        }
    }
    
    static func showMenu() {
        prepareRoot(RNSMenuViewController.initialController)
    }
    
    @discardableResult static func showLogin() -> UIViewController? {
        let vc = RNDLoginController.initialController?.board
        prepareRoot(vc)
        return vc
    }
    
    static func showLoginPullAnimation() {
        let image = STRouter.screenRootViewController
        showLogin()?.preparePullAnimation(image)
    }
    
    static  func showWebTemp() {
        prepareRoot(RNDWebTempController.initialController)
    }
}
