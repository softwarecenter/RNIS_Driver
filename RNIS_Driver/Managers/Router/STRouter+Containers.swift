//
//  STRouter+Containers.swift
//  RNIS
//
//  Created by Артем Кулагин on 31.07.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit

extension STRouter {
    
    static func boardContainer(_ viewController: UIViewController?, isNeedAddTap: Bool = true) -> RNSScrollKeyBoardContainer? {
        return RNSScrollKeyBoardContainer.initController(viewController, isNeedAddTap: isNeedAddTap)
    }
}
