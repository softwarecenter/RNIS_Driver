//
//  RNSMenuManager.swift
//  RNIS
//
//  Created by Артем Кулагин on 01.09.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit
/**
 Контоллер управления боковым меню
 */
class RNSMenuManager: NSObject {
    
    /// Функция обновления бокового меню
    static var handlerLeftMenuUpdate: EmptyBlock?
    /// Функция обнволения меню "Стрелка"
    static var handlerStrelkaUpdate: EmptyBlock?
    /// Функция обнволения меню "Новости"
    static var handlerNewsUpdate: EmptyBlock?
    /// Функция обнволения меню "Избранное"
    static var handlerUpdateFavoriteBuss: EmptyBlock?
    /// Функция очистки данных профиля
    static var handlerClearProfile: EmptyBlock?
    
    static var hanlderUpdateLeftMenu: EmptyBlock?
    
    /// Создание экземпляра менеджера меню
    static let shared = RNSMenuManager()
    
    /// Создание экземпляра раздела меню
    
    static var menuItems: [MenuItem] {
        return shared.menuItems
    }
    
    static var selectedItem: MenuItem?
    
    /// Создание пунктов меню
    lazy var menuItems = [MenuItem("Дашборд", RNSDashViewController.initialController, #imageLiteral(resourceName: "newspaper")),
                     MenuItem("Карта", RNSMapParentController.initialController, #imageLiteral(resourceName: "map"))]
    

    /// Создание "слабого" экзамплеряа контроллера профиля пользователя
    /*
    lazy var profileVC: UIViewController? = {
        return STRouter.imageScrollContainer(RNSProfileViewController.initialController)
    }()
    */
    /// Создание "слабого" экзамплеряа контроллера карты
    /*
    lazy var mapVC: UIViewController? = {
        return RNSMapViewController.controller
    }()
    */
    /// Блок отображения бокового меню
    static var handlerShowLeftMenu: EmptyBlock?
    /// Блок убирания бокового меню
    static var handlerHideLeftMenu: ((Bool) -> Void)?
    /// Блок выбранного контроллера меню
    static var handlerShowVC: ((UIViewController?) -> ())?
    
    /// Функция отображения бокового меню
    static func showLeftMenu() {
        handlerShowLeftMenu?()
    }

    /// Функция убирания бокового меню
    static func hideLeftMenu(animated: Bool = true) {
        handlerHideLeftMenu?(animated)
    }
    
    /// Функция отображения выбранного контроллера меню
    static func selectItem(_ item: MenuItem?) {
        selectedItem = item
        hanlderUpdateLeftMenu?()
        showVC(item?.vc)
    }
    
    static func showVC(_ vc: UIViewController?) {
        handlerShowVC?(vc)
    }
    
    /// Функция показа первого пункта меню
    
    static func showFirst() {
        selectItem(menuItems.first)
    }
    
    /// Фукнция показа профиля пользователя
    /*
    static func showProfile() {
        showVC(shared.profileVC)
    }
    */
    /// Функция показа карты
    /*
    static func showMap() {
        showVC(shared.mapVC)
    }
    */
    /// Фукнция обновления бокового меню
    static func leftMenuUpdate() {
        handlerLeftMenuUpdate?()
    }
    
    /// Функция обновления избранного
    static func updateFavoriteBuss() {
        handlerUpdateFavoriteBuss?()
    }
}
