//
//  RNSMapManager.swift
//  RNIS
//
//  Created by Артем Кулагин on 21.07.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit

/**
Контоллер управления картой
 */
class RNSMapManager: NSObject {
    
    static let shared = RNSMapManager()
    
    /// Создание экземпляра карты
    static var mapView: MapView {
        return shared.mapView
    }
    
    /// Создание экземпляра карты RNIS
    var mapView: MapView = RNSMapView()
    
    /// Создание экземпляра местоположения маркера
    static var pinMyLocation: RNSPinMyLocation {
        return shared.pinMyLocation
    }
    
    lazy var pinMyLocation: RNSPinMyLocation = {
        return RNSPinMyLocation()
    }()
    
    
    /// Функция удаления маркера
    static var handlerRemovePinBuild: EmptyBlock?
    /// Функция добавления маршрута
    static var handlerAddRoute: ((PGPolyline?) -> ())?
    /// Функция показы/скрытия контроллера
    static var handlerDismissOldPresentVC: EmptyBlock?
    /// Функция показа информации
    static var handlerShowInfo: ((RNSCoordinateModel?) -> ())?
    
    /// Функция удаления предыдущего маркера
    static func removeOldPinBuild() {
        handlerRemovePinBuild?()
    }
 }
