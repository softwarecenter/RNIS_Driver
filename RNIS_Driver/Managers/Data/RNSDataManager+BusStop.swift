//
//  RNSDataManager+BusStop.swift
//  RNIS
//
//  Created by Артем Кулагин on 10.08.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import Foundation
import RealmSwift
/**
 Контроллер БД Остановок ТС
 */
extension RNSDataManager {
    
    static var busStops: Results<RNSBusStop>? {
        return realm?.objects(RNSBusStop.self)
    }
    
    static func parseBusStopItemsAsync(_ dicts: [AliasDictionary], complete: AliasStringArrayBlock?) {
        DispatchQueue.global(qos: .userInitiated).async {
            let items = parseItems(dicts) as [RNSBusStop]
            let uuids: [String] = items.flatMap{$0.uuid}
            Utils.mainQueue {
                complete?(uuids)
            }
        }
    }
    
    static func parseBusStopItems(_ dicts: [AliasDictionary]) -> [RNSBusStop]? {
        return parseItems(dicts)
    }
    
    static func bussStopsUuids(_ min: PGGeoPoint, center: PGGeoPoint) -> [String]? {
        guard let results = busStops else {
            return nil
        }
        return modelsUuids(Array(results), min: min, center: center)
    }
    
    static func removeAllBusStop() {
        Utils.mainQueue {
            guard let busStops = busStops else {
                return
            }
            write ({
                realm?.delete(busStops)
            })
        }
    }
}
