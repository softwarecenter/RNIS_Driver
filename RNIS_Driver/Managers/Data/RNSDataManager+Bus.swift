//
//  RNSDataManager+Bus.swift
//  RNIS
//
//  Created by Артем Кулагин on 10.08.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import Foundation
import RealmSwift
/**
 Контроллер БД ТС
 */
extension RNSDataManager {
    
    static var buss: Results<RNSBus>? {
        return realm?.objects(RNSBus.self)
    }
    
    static func createStubBusIfNeed() {
        guard buss?.count == 0  else {
            return
        }
        
         write({
            if let items = buss {
                realm?.delete(items)
            }
            
            let item = RNSBus()
            item.generate()
            realm?.add(item)
        })
    }
    
    static func parseBusItemsAsync(_ dicts: [AliasDictionary], complete: AliasStringArrayBlock?) {
        DispatchQueue.global(qos: .userInitiated).async {
            let items = parseItems(dicts) as [RNSBus]
            let uuids: [String] = items.flatMap{$0.uuid}
            Utils.mainQueue {
                complete?(uuids)
            }
        }
    }

    static func bussUuids(_ min: PGGeoPoint, center: PGGeoPoint, maxCount: Int? = nil) -> [String]? {
        guard let results = buss else {
            return nil
        }
        return modelsUuids(Array(results), min: min, center: center, maxCount: maxCount)
    }
    
    static func removeAllBuss() {
        Utils.mainQueue {
            guard let buss = buss else {
                return
            }
            write ({
                realm?.delete(buss)
            })
        }
    }
}
