//
//  RNDDashManager.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 07.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDDashManager: NSObject {
    
    static var editState = false
    static var handlerReloadDocket: EmptyBlock?
    static var handlerGroupAll: EmptyBlock?
    
    static var items: [RNDJawData]? = [RNDJawData]()
    
    static func removeItem(_ item: RNDJawData?) {
        guard let item = item,
            let index = items?.index(of: item) else {
            return
        }
        items?.remove(at: index)
        handlerReloadDocket?()
    }
    
    static func editStateRevert() {
        self.editState = !editState
    }
    
    static func generateItems() {
        items = RNDJawData.items
    }
}
