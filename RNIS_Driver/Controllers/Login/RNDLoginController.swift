//
//  ViewController.swift
//  RNIS_Driver
//
//  Created by Admin on 02.11.17.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDLoginController: UIViewController {

    @IBAction func enterAction(_ sender: Any) {
        let vc = RNSMenuViewController.initialControllerType()
        vc?.preparePushAnimation(self)
        STRouter.prepareRoot(vc)
    }
}

