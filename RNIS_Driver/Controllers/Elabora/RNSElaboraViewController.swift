//
//  RNSElaboraViewController.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNSElaboraViewController: UIViewController {
    @IBOutlet weak var titleView: RNSNavView!
    @IBOutlet weak var groupView: RNSElaboraGroupView!
    @IBOutlet weak var calendarView: RNDCalendar!
    
    var jawData: RNDJawData?
    var item: RNDPlankData?
    var itemCalendar: RNDCalendarData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareCalendar()
        prepareHandlers()
        reloadData()
    }
    
    func reloadData() {
        item?.genElabora()
        reloadTitle()
        groupReload()
    }
    
    func prepareCalendar() {
        calendarView.item = itemCalendar
    }
    
    func groupReload() {
        groupView.prepareItem(jawData, plankData: item, itemCalendar: calendarView.item)
    }
    
    func reloadTitle() {
        self.title = item?.title_meaning
        titleView.titleText = title
        titleView.backgroundColor = jawData?.color
    }
    
    func prepareHandlers() {
        calendarView.handlerForm = { [weak self] in
            self?.reloadData()
        }
    }
}
