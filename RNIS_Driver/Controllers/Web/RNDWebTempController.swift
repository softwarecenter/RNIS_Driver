//
//  RNDWebTempController.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 02.02.2018.
//  Copyright © 2018 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDWebTempController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    let host = "https://rnis.mosreg.ru/tablet"
    //let host = "https://dev-rnis.regeora.ru/tablet"
    //let host = "https://www.google.ru/"
    
    lazy var loaderView: LoaderView = {
        let view = LoaderView()
        view.isUserInteractionEnabled = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadStart()
    }
    
    func loadStart() {
        loaderView.showInView(self.view)
        webView.loadRequest(URLRequest(url: URL(string: host)!))
    }
     
    func removeLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        loaderView.remove()
    }
    
}
