//
//  RNDWebTempController+Delegate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 02.02.2018.
//  Copyright © 2018 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDWebTempController {
    public func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    internal func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        removeLoader()
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        removeLoader()
    }
}
