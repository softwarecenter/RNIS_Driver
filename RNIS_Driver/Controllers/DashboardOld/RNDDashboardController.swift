//
//  Dashboard.swift
//  RNIS_Driver
//
//  Created by Admin on 02.11.17.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDDashboardController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func editAction(_ sender: Any) {
        STRouter.present(RNDDashboardEditController.controller, animated: false)
        
    }
    
    @IBAction func menuAction(_ sender: Any) {
        /*
        let image = view.asImage
        let vc = STRouter.showLogin()
        vc?.preparePullAnimation(image)
         */
       RNSMenuManager.showLeftMenu()
    }
    
    @IBAction func actionPass(_ sender: Any) {
        RNDDashboard3Controller.controller?.pushAnimated()
    }
    
    @IBAction func actionButton(_ sender: Any) {
        
        var category: String?
        var state: State?
        
        switch (sender as! UIView).tag {
        case 1:
            category = "ЖКХ"
            state = .green
            break
        case 2:
            category = "Пассажирские перевозки"
            state = .red
            break
        case 3:
            category = "Школьные перевозки"
            state = .yellow
            break
        case 4:
            category = "ТКО"
            state = .green
            break
        case 5:
            category = "ГУ ГАТН МО"
            state = .red
            break
        case 6:
            category = "Ремонт и содержание дорог"
            state = .red
            break
        default:
            break
        }
        let vc = RNDDashboardDetaledController.initialControllerType()
        vc?.category = category
        vc?.state = state
        vc?.pushAnimated()
    }
    
    deinit {
        print("RNDDashboardController deinit")
    }
}
