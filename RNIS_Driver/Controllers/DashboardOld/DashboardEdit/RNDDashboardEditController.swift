//
//  RNDDashboardEditController.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 28.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDDashboardEditController: UIViewController {

    @IBAction func editAction(_ sender: Any) {
        dismiss(animated: false)
    }
    
    @IBAction func menuAction(_ sender: Any) {
        /*
         let image = view.asImage
         let vc = STRouter.showLogin()
         vc?.preparePullAnimation(image)
         */
        RNSMenuManager.showLeftMenu()
    }
    
    override class var storyboardName: String {
        return "RNDDashboardController"
    }
}
