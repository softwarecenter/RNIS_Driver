//
//  DashboardDetaledController.swift
//  RNIS_Driver
//
//  Created by Admin on 02.11.17.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

enum State: String {
    case green = "Все хорошо", yellow = "Отклонение", red = "Внимание!"
    var icon: UIImage {
        switch self {
        case .green: return #imageLiteral(resourceName: "Green")
        case .yellow: return #imageLiteral(resourceName: "Yellow")
        case .red: return #imageLiteral(resourceName: "Red")
        }
    }
}

class RNDDashboardDetaledController: UIViewController {
    
     @IBOutlet private weak var stateImageView: UIImageView!
    @IBOutlet private weak var stateLabel: UILabel!
    @IBOutlet private weak var categoryLabel: UILabel!
    //
    var category: String?
    var state: State?

    override func viewDidLoad() {
        super.viewDidLoad()
        stateLabel.text = state?.rawValue
        stateImageView.image = state?.icon
        categoryLabel.text = category
    }
    
    @IBAction func backAction(_ sender: Any) {
        STRouter.pop()
    }
}
