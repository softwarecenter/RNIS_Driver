//
//  RNDDashboard6Controller.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 28.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDDashboard6Controller: UIViewController {

    @IBAction func backAction(_ sender: Any) {
        STRouter.pop()
    }
    
    @IBAction func actionPass(_ sender: Any) {
        dismiss(animated: false)
    }
    
    override class var storyboardName: String {
        return "RNDDashboardController"
    }

}
