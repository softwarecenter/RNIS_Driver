//
//  RNDDashboard5Controller.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 28.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDDashboard5Controller: UIViewController {
    @IBAction func voprosAction(_ sender: Any) {
        STRouter.present(RNDDashboard6Controller.controller, animated: false)
    }
    
    @IBAction func backAction(_ sender: Any) {
        STRouter.pop()
    }
        
    override class var storyboardName: String {
        return "RNDDashboardController"
    }

    @IBAction func colonkaAction(_ sender: Any) {
        STRouter.present(RNDDashboard7Controller.controller, animated: false)
    }
}
