//
//  MenuTableViewCell.swift
//  menuInfo
//
//  Created by Ilya Inyushin on 30.08.17.
//  Copyright © 2017 Ilya Inyushin. All rights reserved.
//

import UIKit

/// Табличная ячейка для меню
class MenuTableViewCell: RNSBaseTableCell {
    
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellImageView: UIImageView!
    
    var item: MenuItem? {
        didSet {
            updateUI()
        }
    }
    
    var defaultImage: UIImage? {
        return item?.image
    }
    
    func updateUI() {
        cellTitle.text = item?.title
        cellImageView.image = item?.image
    }
    
    func updateSelected() {
        let selected = RNSMenuManager.selectedItem == item
        let color: UIColor = selected ? .white : .color4F4F4F
        cellTitle.textColor = color
        cellImageView.tintColor = color
        contentView.backgroundColor = selected ? .FC5C53 : .white
    }
}
