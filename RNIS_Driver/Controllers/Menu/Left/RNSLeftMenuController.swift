//
//  MenuViewController.swift
//  menuInfo
//
//  Created by Ilya Inyushin on 30.08.17.
//  Copyright © 2017 Ilya Inyushin. All rights reserved.
//

import UIKit

/**
 Контроллер меню
 */
class RNSLeftMenuController: UIViewController {
    
    /// Массив моделей пунктов меню
    var menuItems: [MenuItem] {
        return RNSMenuManager.menuItems
    }

    @IBOutlet weak var tableView: UITableView!
    /// Индикатор загрузки
    lazy var loaderView = LoaderView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
        prepareHandlers()
    }
    
    func prepareHandlers() {
        RNSMenuManager.hanlderUpdateLeftMenu = { [weak self] in
            self?.updateCellSelected()
        }
    }
    
    /// Настройка представлений
    func prepareUI() {
        tableView.tableFooterView = UIView();
    }
}
