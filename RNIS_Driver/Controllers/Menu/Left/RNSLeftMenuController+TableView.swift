//
//  RNSLeftMenuController+TableView.swift
//  RNIS
//
//  Created by Артем Кулагин on 01.09.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import Foundation

/// Расширение для обработки событий таблицы
extension RNSLeftMenuController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count;
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as MenuTableViewCell;
        cell.item = self.item(indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        item(indexPath).show()
        updateCellSelected()
    }
    
    func item(_ indexPath: IndexPath) -> MenuItem {
        return menuItems[indexPath.row]
    }
    
    func updateCellSelected() {
        (tableView.visibleCells as? [MenuTableViewCell])?.forEach{
            $0.updateSelected()
        }
    }
}
