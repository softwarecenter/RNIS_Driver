//
//  RNDPickerController+Delegate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 30.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDPickerController: UIPickerViewDelegate,UIPickerViewDataSource {

    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return items.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return item(row)?.text
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        handlerData?(item(row))
        dismiss(animated: false)
    }
    
    func item(_ row: Int) -> RNDEdgeDate? {
        return items.valueAt(row)
    }
    
}
