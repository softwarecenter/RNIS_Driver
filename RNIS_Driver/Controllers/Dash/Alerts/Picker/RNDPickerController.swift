//
//  RNDPickerController.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 30.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDPickerController: UIViewController {

    @IBOutlet weak var datePicker: UIPickerView!
    var type: DiapazonType = .year
    var items = [RNDEdgeDate]()
    typealias AliasTwoDateBlock = (RNDEdgeDate?) -> ()
    var handlerData: AliasTwoDateBlock?
    var startDate: Date?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareItems()
        prepareStartDate()
    }
    
    func prepareItems() {
        guard let component = type.component else {
            return
        }
        let startDate = Date(timeIntervalSince1970: 0)
        var date = Date()
        while startDate.compare(date) == ComparisonResult.orderedAscending, items.count < 1000 {
            if let item = type.itemEdge(date) {
                items.insert(item, at: 0)
            }
            guard let prevousDate = date.dateByMinusUnit(component.0, value: component.1) else {
                break
            }
            date = prevousDate
        }
        datePicker.reloadAllComponents()
        datePicker.selectRow(items.count - 1, inComponent: 0, animated: false)
    }
    
    func prepareStartDate() {
       for (index, item) in items.reversed().enumerated() {
            if item.contains(startDate) {
                prepareCurrent(items.count - 1 - index)
                return
            }
        }
        prepareCurrent(0)
    }
    
    func prepareCurrent(_ index: Int) {
        datePicker.selectRow(index, inComponent: 0, animated: false)
    }
 }
