//
//  RNDAlertCalendarDate+UI.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 30.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation
import CVCalendar

extension RNDAlertCalendarDate {
    
    func prepareCalendarView() {
        prepareStartDate()
        updateYear()
    }
    
    func updateYear() {
        label.text = presentedDate?.stringMMMMyyyy
    }
    
    func prepareStartDate() {
        prepareToggleDate(startDate)
    }
    
    func prepareToggleDate(_ date: Date?) {
        guard let date = date else {
            return
        }
        calendarView.toggleViewWithDate(date)
    }
}
