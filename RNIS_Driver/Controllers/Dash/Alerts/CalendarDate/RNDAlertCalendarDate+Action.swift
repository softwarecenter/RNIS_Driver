//
//  RNDAlertCalendarDate+Action.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 30.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDAlertCalendarDate {
    
    @IBAction func rightAction(_ sender: Any) {
        calendarView.loadNextView()
    }
    
    @IBAction func leftAction(_ sender: Any) {
        calendarView.loadPreviousView()
    }
}
