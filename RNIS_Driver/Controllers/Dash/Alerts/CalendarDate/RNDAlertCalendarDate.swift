//
//  RNDAlertCalendarDate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 29.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit
import CVCalendar

class RNDAlertCalendarDate: UIViewController,MenuViewDelegate {
    
    @IBOutlet weak var label: UILabel!
    
    var beforeDate: Date? = Date(timeIntervalSince1970: 0)
    var beyondDate: Date? = Date()
    var startDate: Date?
    var handlerSelect: AliasDateBlock?
    
    @IBOutlet weak var menuView: CVCalendarMenuView!
    @IBOutlet weak var calendarView: CVCalendarView!

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        menuView?.commitMenuViewUpdate()
        calendarView?.commitCalendarViewUpdate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareCalendarView()
    }
    
    var presentedDate: Date? {
        return calendarView.presentedDate.convertedDate()
    }
}
