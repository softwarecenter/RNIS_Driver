//
//  RNDAlertCalendarDate+Delegate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 29.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation
import CVCalendar

extension RNDAlertCalendarDate: Delegate {
    
    func presentationMode() -> CalendarMode {
        return .monthView
    }
    
    func firstWeekday() -> Weekday {
        return .monday
    }
    
    @objc func shouldAutoSelectDayOnMonthChange() -> Bool {
        return false
    }
    
    @objc func presentedDateUpdated(_ date: CVDate) {
        updateYear()
    }
    
    @objc func didSelectDayView(_ dayView: DayView, animationDidFinish: Bool) {
        handlerSelect?(dayView.date.convertedDate())
        dismiss(animated: true)
    }
    
    @objc func shouldSelectDayView(_ dayView: DayView) -> Bool {
        guard dayView.date != nil,
            let date = dayView.date.convertedDate() ,
            let beforeDate = beforeDate,
            let beyondDate = beyondDate else {
                return true
        }
        if  beforeDate.compare(date) == .orderedDescending ||
            date.compare(beyondDate) == .orderedDescending {
            return false
        }
        return true
    }
}
