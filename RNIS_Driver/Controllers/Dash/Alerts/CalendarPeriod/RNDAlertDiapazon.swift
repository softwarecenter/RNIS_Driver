//
//  RNDCalendarPeriodController.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 29.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDAlertDiapazon: UIAlertController {
    
    var complete: AliasDiapazonType?
    
    @discardableResult static func controller(complete: AliasDiapazonType?) ->  RNDAlertDiapazon {
        let vc = RNDAlertDiapazon(title: "Выберите диапазон", message: nil, preferredStyle: .actionSheet)
        vc.complete = complete
        vc.prepareUI()
        return vc
    }
    
    /// Настройка представлений
    func prepareUI() {
        DiapazonType.items.forEach(addAction)
    }
    
    func addAction(_ type: DiapazonType) {
        addAction(UIAlertAction(title: type.rawValue, style: .default) { [weak self] action in
            self?.complete?(type)
        })
    }

}
