//
//  RNSDashViewController+Edit.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 07.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNSDashViewController {
    
    var editState: Bool {
        return RNDDashManager.editState
    }
    
    func updateStateEditButton() {
        let image = editState ? #imageLiteral(resourceName: "editSelected") : #imageLiteral(resourceName: "editNoSelected")
        editButton.setImage(image, for: .normal)
    }
    
    func updateEditGroupView() {
        if editState {
            groupView.startEditAnimate { [weak self] in
                self?.showLurk()
            }
        } else {
            groupView.endEdit()
            lurkView?.removeFromSuperview()
            labelEdit.removeFromSuperview()
        }
    }
    
    func updateCollectionAnimate(_ complete: EmptyBlock? = nil) {
        groupView.updateCollectionAnimate(complete)
    }
    
    func updateDockedItems() {
        groupView.reloadDockedAndHeight{[weak self] in
            self?.updateLurk()
        }
    }
}
