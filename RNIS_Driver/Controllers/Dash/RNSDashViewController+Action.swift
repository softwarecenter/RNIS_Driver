//
//  RNSDashViewController+Action.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 07.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNSDashViewController {
    
    @IBAction func editAction(_ sender: Any) {
        RNDDashManager.editStateRevert()
        updateStateEditButton()
        updateEditGroupView()
    }
}
