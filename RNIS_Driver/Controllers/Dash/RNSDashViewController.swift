//
//  RNSDashViewController.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 29.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNSDashViewController: UIViewController {
    
    @IBOutlet weak var titleView: RNSNavView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var groupView: RNSDashGroupView!
    @IBOutlet weak var calendarView: RNDCalendar!
    
    var lurkView: STLurkView?
    
    lazy var labelEdit: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .robotoMedium18
        label.text = "Задержите палец на строке, которую требуется передвинуть \n Нижняя строка станет главной в малой карточке"
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleView.titleText = title
        prepareHandlers()
        groupReload()
    }
    
    func groupReload()  {
        groupView.itemCalendar = calendarView.item
    }
}
