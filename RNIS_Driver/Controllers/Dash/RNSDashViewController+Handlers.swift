//
//  RNSDashViewController+Handlers.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 08.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNSDashViewController {
    
    func prepareHandlers() {
        RNDDashManager.handlerReloadDocket = { [weak self] in
            self?.updateDockedItems()
        }
        
        calendarView.handlerForm = { [weak self] in
            self?.groupReload()
        }
    }
}
