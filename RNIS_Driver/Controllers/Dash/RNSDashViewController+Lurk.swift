//
//  RNSDashViewController+Lurk.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 07.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNSDashViewController {
    
    func showLurk() {
        let view = STLurkView(view: self.view)
        self.lurkView = view
        updateLurk()
        view.showAnimate()
        showLabelEdit()
    }
    
    func showLabelEdit() {
        view.addSubview(labelEdit)
        labelEdit.snp.makeConstraints {
            $0.left.right.equalTo(view).inset(15)
            $0.top.equalTo(view).inset(86)
        }
    }
    
    func updateLurk() {
        var rects = groupView.rectsDocket
        let rectButton = STLurkRect.circle(editButton.frame)
        rects?.append(rectButton)
        self.lurkView?.rects = rects
    }
}
