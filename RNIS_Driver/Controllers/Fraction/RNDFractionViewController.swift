//
//  RNDFractionViewController.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 14.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDFractionViewController: UIViewController {
    @IBOutlet weak var titleView: RNSNavView!
    @IBOutlet weak var groupView: RNSFractionGroupView!
    @IBOutlet weak var calendarView: RNDCalendar!
    var item: RNDJawData?
    var itemCalendar: RNDCalendarData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareCalendar()
        prepareItem()
        prepareHandlers()
        groupReload()
    }
    
    func prepareCalendar() {
        calendarView.item = itemCalendar
    }
    
    func groupReload()  {
        groupView.prepareItem(item, itemCalendar: calendarView.item)
    }
    
    func prepareItem() {
        titleView.titleText = title
        titleView.backgroundColor = item?.color
    }
}
