//
//  RNDFractionViewController+Handlers.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 15.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDFractionViewController {
    
    func prepareHandlers() {
        calendarView.handlerForm = { [weak self] in
            self?.groupReload()
        }
    }
}
