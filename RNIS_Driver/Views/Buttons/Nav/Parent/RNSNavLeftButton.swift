//
//  RNSNavLeftButton.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 14.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNSNavLeftButton: UIButton {

    var image: UIImage? {
        return UIImage()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setImage(image?.withRenderingMode(.alwaysOriginal), for: .normal)
        prepareConstraint()
        touchUpInside { [weak self] in
            self?.prepareDefaultAction()
        }
    }
    
    /// Настройка обработчика по-умолчанию
    func prepareDefaultAction() {
        
    }
    
    /// Настройка ограничителей
    func prepareConstraint() {
        guard let view = superview else {
            return
        }
        snp.makeConstraints {
            $0.height.equalTo(heightNav)
            $0.width.equalTo(heightNav)
            $0.left.top.equalTo(view)
        }
    }

}
