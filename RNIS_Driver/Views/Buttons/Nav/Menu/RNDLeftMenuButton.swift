//
//  RNDLeftMenuButton.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 29.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDLeftMenuButton: RNSNavLeftButton {

    override var image: UIImage? {
        return #imageLiteral(resourceName: "burger")
    }
  
    override func prepareDefaultAction() {
        RNSMenuManager.showLeftMenu()
    }
}
