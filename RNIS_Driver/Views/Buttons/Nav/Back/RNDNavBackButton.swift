//
//  RNDNavBackButton.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 14.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDNavBackButton: RNSNavLeftButton {
    override var image: UIImage? {
        return #imageLiteral(resourceName: "arrowLeftBack")
    }
    
    override func prepareDefaultAction() {
        STRouter.pop()
    }
}
