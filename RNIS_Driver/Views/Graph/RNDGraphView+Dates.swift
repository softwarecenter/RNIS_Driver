//
//  RNDGraphView+Dates.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 12.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDGraphView {
    
    func dateView(_ index: Int) -> UIView {
        let view = UILabel()
        view.font = .robotoRegular14
        view.text = item(index)?.dateString
        view.sizeToFit()
        view.center = CGPoint(x: originX(index), y: heightDown + 37)
        view.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/4)
        return view
    }
    
    func prepareDates() {
        removeDates()
        addDates()
    }
    
    func addDates() {
        guard lastIndex > 0 else {
            return
        }
        for i in 0...lastIndex {
            addDate(i)
        }
    }
    
    func addDate(_ index: Int) {
        let view = dateView(index)
        addSubview(view)
        dates.append(view)
    }
    
    func removeDates() {
        dates.forEach { $0.removeFromSuperview() }
        dates = []
    }
}
