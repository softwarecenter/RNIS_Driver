//
//  RNSSubstrateChart.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 12.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNSSubstrateChart: UIView {
    
    var views = [UIView]()
    var edgeTop: CGFloat = 38
    let edgeLeft: CGFloat = 52
    let edgeBottom: CGFloat = 76
    let edgeRight: CGFloat = 31
    let countLine = 10
    var between: CGFloat = 0
    var item: RNDGraphData?
    
    func removeAll() {
        views.forEach { $0.removeFromSuperview() }
        views = []
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        draw()
    }
    
    func prepareItem(_ item: RNDGraphData?, edgeTop: CGFloat) {
        self.item = item
        self.edgeTop = edgeTop
    }
}
