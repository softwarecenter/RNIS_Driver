//
//  RNSSubstrateChart+Draw.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 13.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNSSubstrateChart {
    
    func draw() {
        prepareKoef()
        removeAll()
        drawLines()
        drawCadre()
        drawLabels()
    }
    
    func viewDraw(_ rect: CGRect) {
        let view = UIView(frame: rect)
        view.backgroundColor = .F2F2F2
        addSubview(view)
        views.append(view)
    }
    
    func viewDrawVertical(_ y: CGFloat, depth: CGFloat) {
        viewDraw(CGRect(x: edgeLeft, y: y, width: width - edgeLeft - edgeRight, height: depth))
    }
    
    func drawCadre() {
        let depth: CGFloat = 2
        viewDraw(CGRect(x: edgeLeft, y: edgeTop, width: depth, height: heightChart))
        viewDrawVertical(heightBottom, depth: depth)
    }
    
    func drawLines() {
        for i in 0...countLine {
            viewDrawVertical(originY(i), depth: CGFloat(1))
        }
    }
    
    func addLabel(_ index: Int, text: String) {
        let view = UILabel()
        view.font = .robotoRegular12
        view.textColor = .color9C9C9C
        view.text = text
        view.sizeToFit()
        let originX = edgeLeft - 5 - view.frame.width/2
        view.center = CGPoint(x: originX, y: originY(index))
        addSubview(view)
        views.append(view)
    }
    
    func drawLabels() {
        guard let maxValue = item?.maxValue else {
            return
        }
        let step = maxValue/10
        for i in 0...countLine - 1  {
            let text = (maxValue - step * CGFloat(i)).stringComma
            addLabel(i, text: text)
        }
        addLabel(countLine, text: "0")
    }
}
