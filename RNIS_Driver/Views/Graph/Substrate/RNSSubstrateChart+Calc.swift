//
//  RNSSubstrateChart+Var.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 13.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNSSubstrateChart {
    
    var height: CGFloat {
        return frame.height
    }
    
    var width: CGFloat {
        return frame.width
    }
    
    var heightBottom: CGFloat {
        return height - edgeBottom
    }
    
    var heightChart: CGFloat {
        return heightBottom - edgeTop
    }
     
    func originY(_ i: Int) -> CGFloat  {
        return edgeTop + between * CGFloat(i)
    }
    
    func prepareKoef() {
        between = heightChart/CGFloat(countLine)
    }
}
