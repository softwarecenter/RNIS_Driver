//
//  RNDGraphView+Lines.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 12.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDGraphView {
    
    func lineView(_ index: Int) -> UIView {
        let view = UIView(frame: CGRect(x: originX(index) - 1, y: heightDown, width: 2, height: 12))
        view.backgroundColor = .F2F2F2
        return view
    }
    
    func prepareLines() {
        removeLines()
        addLines()
    }
    
    func addLines() {
        guard lastIndex > 0 else {
            return
        }
        for i in 0...lastIndex {
            addLine(i)
        }
    }
    
    func addLine(_ index: Int) {
        let view = lineView(index)
        addSubview(view)
        lines.append(view)
    }
    
    func removeLines() {
        lines.forEach { $0.removeFromSuperview() }
        lines = []
    }
}
