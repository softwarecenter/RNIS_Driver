//
//  RNDGraphView+Var.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 11.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDGraphView {
    
    var countItems: Int {
        return items?.count ?? 0
    }
    
    var lastIndex: Int {
        var lastIndex = countItems - 1
        if lastIndex < 0 {
            lastIndex = 0
        }
        return lastIndex
    }
     
    var items: [RNDGraphItem]? {
        return item?.items
    }
    
    var height: CGFloat {
        return heightOptional ?? 60
    }
    
    var heightDown: CGFloat {
        return height - edgeDown
    }
    
    var heightPlot: CGFloat {
        return height - edgeDown - edgeTop
    }
    
    var lastPoint: CGPoint {
        return point(lastIndex)
    }
    
    var widthAllGraph: CGFloat {
        return lastPointX + edgeRight
    }
    
    var lastPointX: CGFloat {
        return lastPoint.x
    }
}
