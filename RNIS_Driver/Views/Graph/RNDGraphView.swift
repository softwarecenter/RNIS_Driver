//
//  GraphView.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 08.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

class RNDGraphView: BaseViewWithXIBInit {
    
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelEnd: UILabel!
    @IBOutlet weak var labelXConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelYConstraint: NSLayoutConstraint!
    
    var item: RNDGraphData?
    var koefHeight: CGFloat = 1
    var betweenLine: CGFloat = 60
    var edgeTop: CGFloat = 38
    let edgeLeft: CGFloat = 17
    let edgeRight: CGFloat = 31
    let edgeDown: CGFloat = 56
    var heightLine: CGFloat  = 0
    var graphLayer:CALayer?
    var heightOptional: CGFloat?
    var externalEdge: CGFloat = 106
    var dots = [UIView]()
    var lines = [UIView]()
    var dates = [UIView]()
    var colorGraph = UIColor.graph
    
    func prepareItem(_ item: RNDGraphData?, height: CGFloat?, edgeTop: CGFloat, colorGraph: UIColor? = UIColor.graph) {
        self.item = item
        self.heightOptional = height
        self.edgeTop = edgeTop
        self.colorGraph = colorGraph ?? .graph
        loadItems()
    }
    
    func loadItems() {
        prepareKoef()
        prepareLines()
        prepareGraphLine()
        prepareDots()
        prepareEndLabel()
        prepareDates()
        widthConstraint.constant = widthAllGraph
    }
    
    func prepareEndLabel() {
        labelXConstraint.constant = lastPoint.x
        let pointY = lastPoint.y
        let different: CGFloat = pointY < (edgeTop + 6) ? 24 : 30
        labelYConstraint.constant = pointY - different
        labelEnd.text = CGFloat(items?.last?.value ?? 0).stringComma
    }
}
