//
//  RNDGraphView+Draw.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 08.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDGraphView {
    
    var graphLine:UIBezierPath {
        let path = UIBezierPath()
        let startPoint = CGPoint(x: edgeLeft, y: heightDown)
        path.move(to: startPoint)
        for i in 0...lastIndex {
            let point = self.point(i)
            path.addLine(to: point)
        }
        path.addLine(to: CGPoint(x: lastPointX, y: heightDown))
        path.addLine(to: startPoint)
        return path
    }
    
    func prepareGraphLine() {
        graphLayer?.removeFromSuperlayer()
        if countItems == 0 {return}
        addGraphLine()
    }
    
    func addGraphLine() {
        let layer = CAShapeLayer()
        layer.path = graphLine.cgPath
        layer.fillColor = colorGraph.withAlphaComponent(0.45).cgColor
        graphLayer = layer
        self.layer.addSublayer(layer)
    }
}
