//
//  RNDGraphScroll.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 11.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

class RNDGraphScroll: BaseViewWithXIBInit, UIScrollViewDelegate {
    @IBOutlet weak var graphView: RNDGraphView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var substrateChart: RNSSubstrateChart!
    let edgeTop: CGFloat = 38
    
    convenience init(_ item: RNDGraphData?, height: CGFloat?, colorGraph: UIColor? = UIColor.graph) {
        self.init()
        
        let donwUpGraph: CGFloat = 20
        graphView.prepareItem(item, height: (height ?? 0) - donwUpGraph, edgeTop: edgeTop, colorGraph: colorGraph)
        substrateChart.prepareItem(item, edgeTop: edgeTop)
    }
}
