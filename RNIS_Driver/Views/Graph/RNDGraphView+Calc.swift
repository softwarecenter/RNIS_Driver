//
//  RNDGraphView+Culc.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 08.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDGraphView {
    
    func point(_ index: Int) -> CGPoint {
        return CGPoint(x: originX(index), y: originY(index))
    }
    
    func originX(_ index: Int) -> CGFloat {
        return edgeLeft + (betweenLine) * CGFloat(index)
    }
    
    func originY(_ index: Int) -> CGFloat  {
        let value = item(index)?.value ?? 0
        return heightDown - value * koefHeight
    }
    
    func item(_ index: Int) -> RNDGraphItem? {
        return items?.valueAt(index)
    }
    
    func prepareKoef() {
        guard let item = item else {
            return
        }
        koefHeight = heightPlot/item.maxValue
        prepareBetweenLine()
    }
    
    func prepareBetweenLine() {
        let widthChart = UIScreen.width - edgeLeft - edgeRight - externalEdge
        let betweenLine = widthChart/CGFloat(lastIndex)
        if betweenLine > 60 {
            self.betweenLine = CGFloat(Int(betweenLine))
        }
    }
}
