//
//  RNDGraphView+Dots.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 11.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDGraphView {
    
    func prepareDots() {
        removeDots()
        addDots()
    }
    
    func addDots() {
        guard lastIndex > 0 else {
            return
        }
        for i in 0..<lastIndex {
            addDot(i)
        }
        addDot(lastIndex, black: true)
    }
    
    func addDot(_ index: Int, black: Bool = false) {
        let view = UIImageView(image: black ? #imageLiteral(resourceName: "dotBlack") : #imageLiteral(resourceName: "dotBlue"))
        view.center = point(index)
        addSubview(view)
        dots.append(view)
    }
    
    func removeDots() {
        dots.forEach { $0.removeFromSuperview() }
        dots = []
    }
}
