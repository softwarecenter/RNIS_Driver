//
//  RNSGroupStackView.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 06.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNSGroupView: BaseViewWithXIBInit {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!
    let topInsets: CGFloat = 30
    
    lazy var items: [UIView] = {
        return self.itemsValue
    }()
    
    var itemsValue: [UIView] {
        return [UIView]()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        scrollView.contentInset = UIEdgeInsetsMake(topInsets, 0, 0, 0);
        prepareStackView()
    }
    
    func prepareStackView() {
        items.forEach {
            stackView.addArrangedSubview($0)
        }
    }
    
    override var nibNamed:String {
        return Utils.stringFromSwiftClass(RNSGroupView.self)
    }
}
