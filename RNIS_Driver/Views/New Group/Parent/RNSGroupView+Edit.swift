//
//  RNSGroupView+Edit.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 07.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNSGroupView {
    
    func scrollEnabled(_ enabled: Bool) {
        scrollView.isScrollEnabled = enabled
    }
    
    func goToStart(_ complete: EmptyBlock?) {
        let startOffset = -topInsets
        if scrollView.contentOffset.y == startOffset {
            complete?()
            return
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.scrollView.contentOffset.y = startOffset
        }) { _ in
            complete?()
        }
    }

}
