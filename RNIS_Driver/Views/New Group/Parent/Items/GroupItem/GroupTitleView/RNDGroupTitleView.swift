//
//  RNDGroupTitleView.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 06.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDGroupTitleView: BaseViewWithXIBInit {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    var handlerAction: EmptyBlock?
    
    @IBInspectable var titleText: String? {
        didSet {
            titleLabel.text = titleText
        }
    }
    
    func prepareOpen(_ open: Bool) {
        imageView.image = open ? #imageLiteral(resourceName: "groupArrowUp"):#imageLiteral(resourceName: "groupArrowDown")
        titleLabel.textColor = open ? .black : .color4F4F4F
    }
}
