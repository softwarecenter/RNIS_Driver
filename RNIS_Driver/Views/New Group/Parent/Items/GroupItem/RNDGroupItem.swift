//
//  RNDGroupItem.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 06.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDGroupItem: BaseViewWithXIBInit {
    
    @IBOutlet weak var heightConstaint: NSLayoutConstraint!
    
    var heightClose: CGFloat = 60
    var heightOpen: CGFloat = 60
    var open: Bool = false
    
    var title: String? = "defaultTitle"
    @IBOutlet weak var titleView: RNDGroupTitleView!
    
    init(_ title: String? = nil, open: Bool = false, heightOpen: CGFloat = 60) {
        super.init(frame: CGRect.zero)
        self.open = open
        self.heightOpen = heightOpen
        self.title = title
        prepare()
    }
    
    required init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func prepare() {
        prepareTitleView()
        prepareHandlers()
        changeHeight()
    }
    
    func prepareTitleView() {
        prepareTitle(title)
        updateOpenStateTitleView()
    }
    
    func prepareTitle(_ title: String?){
        titleView.titleText = title
    }
    
    func updateOpenStateTitleView() {
        titleView.prepareOpen(open)
    }
    
    func prepareHandlers() {
        titleView.tapGesture { [weak self] in
            self?.updateOpen()
        }
    }
    
    func updateOpen() {
        self.open = !open
        updateOpenStateTitleView()
        changeHeight()
    }

    func changeHeight() {
        self.heightConstaint.constant = open ? heightOpen : heightClose
    }

    override var nibNamed:String {
        return Utils.stringFromSwiftClass(RNDGroupItem.self)
    }
}
