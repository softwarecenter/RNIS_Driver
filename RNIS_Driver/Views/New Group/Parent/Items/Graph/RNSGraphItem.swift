//
//  RNSGraphView.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 06.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNSGraphItem: RNDGroupItem {
    
    var graphView: UIView?
    var color: UIColor? = UIColor.graph
    var item: RNDGraphData? {
        didSet {
           updateGraph()
        }
    }
        
    convenience init() {
        self.init(open: true, heightOpen: 386)
        updateGraph()
    }
    
    func prepareGraph() {
        removeGraph()
        let view = RNDGraphScroll(item, height: heightOpen - heightClose, colorGraph: color)
        addSubview(view)
        view.snp.makeConstraints {
            $0.top.equalTo(self.titleView.snp.bottom)
            $0.bottom.left.right.equalTo(self)
        }
        graphView = view
    }
    
    override func updateOpen() {
        super.updateOpen()
        updateGraph()
    }
    
    func updateGraph() {
        if open {
            prepareGraph()
        } else {
            removeGraph()
        }
    }
    
    func removeGraph() {
        graphView?.removeFromSuperview()
    }
}
