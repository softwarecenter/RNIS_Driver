//
//  RNSFractionGroupView.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 14.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNSFractionGroupView: RNSGroupView {
    let graphItem = RNSGraphItem()
    let collection = RNDMeanCollection()
    
    var item: RNDJawData?
    var itemCalendar: RNDCalendarData?
    
    func prepareItem(_ item: RNDJawData?, itemCalendar: RNDCalendarData?) {
        self.item = item
        self.itemCalendar = itemCalendar
        reloadAll()
    }
   
    override var itemsValue: [UIView] {
        return [RNDPathNav(),
                collection,
                graphItem,
                RNDGroupItem("Сводный график оснащённости ГЛОНАСС"),
                RNDGroupItem("Сводный график возраста автопарка"),
                RNDGroupItem("Сводный график выполнения транспортной работы")]
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        prepareGraph()
    }
    
    func prepareGraph() {
        graphItem.prepareTitle("Сводный график изменения рейтинга")
    }
    
    func reloadAll() {
        reloadGraph()
        collectionReload()
    }
    
    func reloadGraph() {
        graphItem.color = item?.color
        graphItem.item = RNDGraphData.generate
    }
    
    func collectionReload() {
        collection.prepareItem(item, itemCalendar: itemCalendar)
    }
}
