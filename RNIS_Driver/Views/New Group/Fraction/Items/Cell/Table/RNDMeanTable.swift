//
//  RNDMeanTable.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDMeanTable: BaseViewWithXIBInit {
    
    @IBOutlet var tableView: RNSRegisterTableView!
    
    var items: [RNDCompanyData]?
    var isPercent: Bool?
    
    func prepareItem(_ items: [RNDCompanyData]?, isPercent: Bool?) {
        self.items = items
        self.isPercent = isPercent
        tableView.reloadData()
    }
}
