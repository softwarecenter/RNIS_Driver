//
//  RNDCompanyCell.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDCompanyCell: RNSBaseTableCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    var item: RNDCompanyData?
    var isPercent: Bool?
    
    func prepareItem(_ item: RNDCompanyData? , isPercent: Bool?) {
        self.item = item
        self.isPercent = isPercent
        prepareItem()
    }
   
    func prepareItem() {
        titleLabel.text = item?.name
        valueLabel.text = item?.value?.evolutePercent(isPercent, round: false)
    }
}
