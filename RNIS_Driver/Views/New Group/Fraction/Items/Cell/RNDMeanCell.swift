//
//  RNDMeanCell.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

class RNDMeanCell: UICollectionViewCell {
    var type: String?
    var itemCalendar: RNDCalendarData?
    var item: RNDPlankData?
    
    @IBOutlet weak var bestMeanTable: RNDMeanTable!
    @IBOutlet weak var worstMeanTable: RNDMeanTable!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var labelValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        prepareShadow()
        labelValue.roundCorners([.topRight], radius: 3)
    }
    
    func prepareItem(_ item: RNDPlankData?, type: String?, itemCalendar: RNDCalendarData?) {
        self.item = item
        self.type = type
        self.itemCalendar = itemCalendar
        self.item?.generateDetail()
        prepareItem()
    }
    
    func prepareItem() {
        titleLabel.text = item?.title_detail
        labelValue.text = item?.textValue
        labelValue.backgroundColor = item?.color
        let isPercent = item?.isPercent
        bestMeanTable.prepareItem(item?.best, isPercent: isPercent)
        worstMeanTable.prepareItem(item?.worst, isPercent: isPercent)
    }
}
