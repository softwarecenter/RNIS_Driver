//
//  RNDMeanCollection+Delegate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDMeanCollection: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as RNDMeanCell
        cell.prepareItem(item(indexPath), type: type, itemCalendar: itemCalendar)
        return cell
    }
    
    func item(_ indexPath: IndexPath?) -> RNDPlankData? {
        guard let indexPath = indexPath else {
            return nil
        }
        return items?[indexPath.row]
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = RNSElaboraViewController.initialControllerType()
        let plankData = item(indexPath)
        vc?.title = plankData?.title
        vc?.jawData = item
        vc?.item = plankData
        vc?.itemCalendar = itemCalendar
        vc?.pushAnimated()
    }
}
