//
//  RNDMeanCollection+Height.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDMeanCollection {
    
    var heightView: CGFloat {
        let heightRow: CGFloat = heightCell + between
        let bottomEdge: CGFloat = 10
        guard let items = items else {
            return heightRow * 2 + bottomEdge
        }
        let countRows = CGFloat(lroundf(Float(items.count)/Float(2)))
         return countRows * heightRow + bottomEdge
    }
    
    func updateHeight() {
        self.heightCollection.constant = self.heightView
    }
}
