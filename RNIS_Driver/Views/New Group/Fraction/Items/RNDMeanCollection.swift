//
//  RNDMeanCollection.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDMeanCollection: BaseViewWithXIBInit {
    
    @IBOutlet weak var heightCollection: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let between: CGFloat = 29
    let heightCell: CGFloat = 207
    var externalEdge: CGFloat = 60
    
    var item: RNDJawData?
    var itemCalendar: RNDCalendarData?
    
    var type: String? {
        return item?.type
    }
    
    var items: [RNDPlankData]? {
        return item?.values
    }
    
    override func loadNibView() {
        super.loadNibView()
        
        collectionView.contentInsetEdgeGroup()
        prepareSizeCell()
        reloadCollection()
    }
    
    func prepareItem(_ item: RNDJawData?, itemCalendar: RNDCalendarData?) {
        self.item = item
        self.itemCalendar = itemCalendar
        reloadCollection()
    }
    
    func reloadCollection() {
        collectionView.reloadData()
        Utils.mainQueue {
            self.updateHeight()
        }
    }
    
    func prepareSizeCell() {
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let width = (UIScreen.width - externalEdge - between)/2
            layout.itemSize = CGSize(width: width, height: heightCell)
        }
    }
}
