//
//  RNSDashGroupView.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 14.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNSDashGroupView: RNSGroupView {
    
    let docketCollection = RNSDocketCollection()
    let graphItem = RNSGraphItem()
    
    var itemCalendar: RNDCalendarData? {
        didSet {
            reloadAll()
        }
    }
    
    var rectsDocket: [STLurkRect]? {
        return docketCollection.rectsCells
    }
    
    override var itemsValue: [UIView] {
        return [docketCollection,
                graphItem,
                RNDGroupItem("Сводный график оснащённости ГЛОНАСС по всем перевозчикам"),
                RNDGroupItem("Сводный график возраста автопарка по всем перевозчикам"),
                RNDGroupItem("Сводный график выполнения транспортной работы по всем перевозчикам")]
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        prepareGraph()
    }
    
    func prepareGraph() {
        graphItem.prepareTitle("Сводный график изменения рейтинга по всем перевозчикам")
    }
    
    func reloadDockedAndHeight(_ complete: EmptyBlock? = nil) {
        docketReload()
        updateCollectionAnimate(complete)
    }
    
    func reloadAll() {
        reloadDockedAndHeight()
        reloadGraph()
    }
    
    func reloadGraph() {
        graphItem.item = RNDGraphData.generate
    }
}
