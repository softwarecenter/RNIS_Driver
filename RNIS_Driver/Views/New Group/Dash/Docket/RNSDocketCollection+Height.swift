//
//  RNSDocketCollection+Height.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 07.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNSDocketCollection {
    
    var heightView: CGFloat {
        guard let items = items else {
            return 500
        }
        var height: CGFloat = 0
        var counter: CGFloat = 0
        var heightRow: CGFloat = 0
        for item in items {
            let heightItem = item.height
            if heightItem > heightRow {
                heightRow = heightItem
            }
            counter += 1
            if counter > (self.countItemsInRow - 1){
                counter = 0
                height += heightRow
                heightRow = 0
            }
        }
        height += heightRow
        let countRows = CGFloat(lroundf(Float(items.count)/Float(self.countItemsInRow)))
        height += countRows * self.betweenRow
        return height
    }
    
    func updateCollectionAnimate(_ complete: EmptyBlock? = nil) {
        self.updateHeight()
        collectionView.performBatchUpdates(nil) { value in
            complete?()
        }
    }
    
    func updateHeight() {
        //  UIView.animateConstrains(self) {
        self.heightCollection.constant = self.heightView
        //}
    }
}
