//
//  RNSDocketCollection+Delegate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 06.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNSDocketCollection: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as RNDDocketCollectionCell
        cell.item = item(indexPath)
        cell.itemCalendar = itemCalendar
        cell.handlerUpdateHeight = {[weak self] in
            self?.updateCollectionAnimate()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: widthCell, height: item(indexPath)?.height ?? 227)
    }
    
    func item(_ indexPath: IndexPath?) -> RNDJawData? {
        guard let indexPath = indexPath else {
            return nil
        }
        return items?[indexPath.row]
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         item(indexPath)?.showIfNeed(itemCalendar)
    }
}
