//
//  RNDDocketCollectionCell.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 06.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDDocketCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var developView: RNDDevelopView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var openView: RNDPlancsOpen!
    @IBOutlet weak var closeView: RNDPlancsClose!
    @IBOutlet weak var buttonOpen: UIButton!
    @IBOutlet weak var crossButton: UIButton!
    
    var handlerUpdateHeight: EmptyBlock?
    
    var itemCalendar: RNDCalendarData?
    
    var item: RNDJawData? {
        didSet {
            prepareItem()
            prepareHandlers()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        prepareShadow()
    }
    
    func prepareHandlers() {
        item?.handlerUpdateUI = { [weak self] in
            self?.prepareItem()
        }
    }
}
