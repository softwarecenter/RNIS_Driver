//
//  RNDGroupListCell.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 07.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDPlancsOpen: BaseViewWithXIBInit {
    
    @IBOutlet weak var imageConstaint: NSLayoutConstraint!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tableView: RNSRegisterTableView!
    
    var itemCalendar: RNDCalendarData?
    
    var item: RNDJawData? {
        didSet {
            prepareItem()
        }
    }
    
    var items: [RNDPlankData]? {
        return item?.values
    }
    
    var editState: Bool {
        return RNDDashManager.editState
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tableView.semanticContentAttribute = .forceRightToLeft
    }
    
    func prepareItem() {
        tableView.isEditing = editState
        prepareImage()
        tableView.reloadData()
    }
    
    func prepareImage() {
        imageView.image = item?.image
        imageConstaint.constant = editState ? 19 : 50
    }
}
