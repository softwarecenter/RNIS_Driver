//
//  RNDPlancsOpen+TableView.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 07.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDPlancsOpen: UITableViewDataSource, UITableViewDelegate {
       
    var itemsCount: Int {
        return items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as RNDPlancOpenCell;
        cell.item = item(indexPath)
        cell.last = (itemsCount - 1) == indexPath.row
        return cell
    }
    
    func item(_ indexPath: IndexPath) -> RNDPlankData? {
        return items?[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    public func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let row = sourceIndexPath.row
        guard var items = self.items,
            let item = items.valueAt(row)  else {
            return
        }
        items.remove(at: row)
        items.insert(item, at: destinationIndexPath.row)
        self.item?.values = items
        prepareItem()
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.none
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        item?.showIfNeed(itemCalendar)
    }
}
