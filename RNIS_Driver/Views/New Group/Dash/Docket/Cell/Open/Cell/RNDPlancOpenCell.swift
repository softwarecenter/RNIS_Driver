//
//  RNDGroupOpenCell.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 07.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDPlancOpenCell: RNSBaseTableCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    @IBOutlet weak var titleConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageSandwich: UIImageView!
    
    var item: RNDPlankData? {
        didSet {
            prepareItem()
        }
    }
    
    func prepareItem() {
        titleLabel.text = item?.textValue
        valueLabel.text = item?.title
    }
    
    var last: Bool? {
        didSet {
            prepareFont()
        }
    }
    
    func prepareFont() {
        let font: UIFont = (last ?? false) ? .robotoMedium16: .robotoRegular13
        titleLabel.font = font
        valueLabel.font = font
    }
}
