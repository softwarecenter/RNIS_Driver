//
//  RNDDocketCollectionCell+Action.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 06.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDDocketCollectionCell {
    
    @IBAction func buttonAction(_ sender: Any) {
        item?.openClosed()
        handlerUpdateHeight?()
        updateViews()
    }
    
    @IBAction func crossAction(_ sender: Any) {
        RNDDashManager.removeItem(item)
    }
}
