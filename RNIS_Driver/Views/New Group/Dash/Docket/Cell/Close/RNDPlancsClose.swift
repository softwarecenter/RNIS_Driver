//
//  RNDPlancsClose.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 07.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

class RNDPlancsClose: BaseViewWithXIBInit {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var imageArrow: UIImageView!
    @IBOutlet weak var changeLabel: UILabel!
    
    var item: RNDJawData? {
        didSet {
            prepareItem()
        }
    }
    
    var lastItem: RNDPlankData? {
        return item?.values?.last
    }
    
    func prepareItem() {
        titleLabel.text = lastItem?.title
        valueLabel.text = lastItem?.textValue
        imageView.image = item?.image
        prepareChangeValue()
    }
    
    func prepareChangeValue() {
        guard let change = lastItem?.change else {
            return
        }
        imageArrow.image = (change >= 0) ? #imageLiteral(resourceName: "ArrowLongUp") : #imageLiteral(resourceName: "ArrowLongDown")
        changeLabel.text = lastItem?.textChange
    }
}

