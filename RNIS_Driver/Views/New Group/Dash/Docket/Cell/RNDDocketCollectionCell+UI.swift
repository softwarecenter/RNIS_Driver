//
//  RNDDocketCollectionCell+UI.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 07.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDDocketCollectionCell {
    var isOpen: Bool {
        return item?.openValue ?? true
    }
    
    var inDeveloping: Bool {
        return item?.inDeveloping ?? false
    }
    
    var editState: Bool {
        return RNDDashManager.editState
    }
    
    func prepareItem() {
        guard let item = item else {
            return
        }
        label.text = item.title
        backgroundColor = item.color
        updateViews()
        prepareButtonOpen()
        prepareCrossButton()
    }
    
    func updateViews() {
        hiddenAll()
        if inDeveloping {
            developView.isHidden = false
            return
        }
        if isOpen {
            openView.itemCalendar = itemCalendar
            openView.item = item
        }else {
            closeView.item = item
        }
        openView.isHidden = !isOpen
        closeView.isHidden = isOpen
    }
    
    func hiddenAll() {
        openView.isHidden = true
        closeView.isHidden = true
        developView.isHidden = true
    }
    
    func buttonOpenHidden(_ hidden: Bool) {
        buttonOpen.isHidden = hidden
    }
    
    func prepareButtonOpen() {
        buttonOpenHidden(false)
        if inDeveloping || editState {
            buttonOpenHidden(true)
        }
    }
    
    func prepareCrossButton() {
        crossButton.isHidden = !editState
    }
}
