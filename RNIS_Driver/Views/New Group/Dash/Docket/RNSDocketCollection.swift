//
//  RNSDocketCollection.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 06.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNSDocketCollection: BaseViewWithXIBInit {
    
    @IBOutlet weak var heightCollection: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var items: [RNDJawData]? {
        return RNDDashManager.items
    }
    
    var itemCalendar: RNDCalendarData? {
        didSet {
            RNDDashManager.generateItems()
            reloadCollection()
        }
    }
    
    let countItemsInRow: CGFloat = 3
    let betweenRow: CGFloat = 30
    
    lazy var widthCell: CGFloat = {
        let countBetween = self.countItemsInRow + 1
        return ((UIScreen.width - self.betweenRow * countBetween)/self.countItemsInRow) - 0.01
    }()
    
    override func loadNibView() {
        super.loadNibView()

        collectionView.contentInsetEdgeGroup()
        reloadCollection()
        Utils.mainQueue {
            self.updateHeight()
        }
    }
    
    func reloadCollection() {
        collectionView.reloadData()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateHeight()
    }
    
    var rectsCells: [STLurkRect]? {
        var rects = [STLurkRect]()
        for index in 0 ..< collectionView!.numberOfItems(inSection: 0) {
            let indexPath = IndexPath(item: index, section: 0)
            if let frame = collectionView.layoutAttributesForItem(at: indexPath)?.frame,
                let rootView = STRouter.rootView {
                let rootFrame = self.collectionView.convert(frame, to: rootView)
                rects.append(STLurkRect(rootFrame))
            }
        }
        return rects
    }
    
    func updateItems() {
        items?.forEach {
            $0.updateUI()
        }
    }
}
