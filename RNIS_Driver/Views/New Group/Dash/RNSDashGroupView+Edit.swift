//
//  RNSDashGroupView+Edit.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 14.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNSDashGroupView {
    func startEditAnimate(_ complete: EmptyBlock?) {
        scrollEnabled(false)
        goToStart { [weak self] in
            self?.updateCollectionAnimate(complete)
        }
    }
    
    func endEdit() {
        scrollEnabled(true)
        updateCollectionAnimate()
    }
    
    func updateCollectionAnimate(_ complete: EmptyBlock? = nil) {
        docketCollection.updateCollectionAnimate(complete)
        docketCollection.updateItems()
    }
    
    func docketReload() {
        docketCollection.itemCalendar = itemCalendar
    }
}
