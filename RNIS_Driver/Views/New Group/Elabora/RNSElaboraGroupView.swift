//
//  RNSElaboraGroupView.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 18.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNSElaboraGroupView: RNSGroupView {
    let graphItem = RNSGraphItem()
    let pathNav = RNDPathNav()
    let table = RNDElaboraTableView()
    
    var item: RNDPlankData?
    var jawData: RNDJawData?
    var itemCalendar: RNDCalendarData?
    
    func prepareItem(_ item: RNDJawData?, plankData: RNDPlankData?, itemCalendar: RNDCalendarData?) {
        self.jawData = item
        self.item = plankData
        self.itemCalendar = itemCalendar
        reloadAll()
    }
    
    override var itemsValue: [UIView] {
        return [pathNav,
                table,
                graphItem,
                RNDGroupItem("Сводный график оснащённости ГЛОНАСС"),
                RNDGroupItem("Сводный график возраста автопарка"),
                RNDGroupItem("Сводный график выполнения транспортной работы")]
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        prepareGraph()
    }
    
    func prepareGraph() {
        graphItem.prepareTitle("Сводный график изменения рейтинга")
    }
    
    func reloadAll() {
        table.item = item?.table
        pathNav.reloadData()
        reloadGraph()
    }
    
    func reloadGraph() {
        graphItem.color = jawData?.color
        graphItem.item = RNDGraphData.generate
    }
}
