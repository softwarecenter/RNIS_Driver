//
//  RNDElaboraTableView+Delegate.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 19.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDElaboraTableView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as RNDElaboraTableCell
        cell.prepareItems(item(indexPath), fields: fields)
        return cell
    }
    
    func item(_ indexPath: IndexPath) -> RNDCompanyData? {
        return items?[indexPath.row]
    }
}
