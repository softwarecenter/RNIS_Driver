//
//  RNDTitlesElaboraView.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 19.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDTitlesElaboraView: BaseViewWithXIBInit {
    @IBOutlet weak var fildsView: RNDFieldsElaboraView!
    
    var fields: [RNDElaboraField]? {
        didSet {
            prepareFields()
        }
    }
    
    func prepareFields() {
        fildsView.prepareFields(fields) { (label, key) in
            label.text = self.fields?.first(where: { $0.key == key})?.title
            label.backgroundColor = .color454B52
            label.textColor = .white
            label.font = .robotoMedium16
            if key == kName {
                label.textAlignment = .left
                label.padding = UIEdgeInsets(top: 0, left: 23, bottom: 0, right: 0)
            }
        }
    }
}
