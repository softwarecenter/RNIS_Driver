//
//  RNDFieldsElaboraView.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 19.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDFieldsElaboraView: BaseViewWithXIBInit {
    @IBOutlet weak var stackView: UIStackView!
    
    typealias AliasFieldsBlock = (UILabel,String) -> ()
    
    var label: UILabel {
        let label = UILabel()
        label.font = .robotoRegular14
        label.textAlignment = .center
        label.textColor = .color4F4F4F
        label.numberOfLines = 0
        return label
    }
    
    var fields: [RNDElaboraField]?
    var handlerLabel: AliasFieldsBlock?
    
    var summKoef: CGFloat {
        return fields?.summKoef ?? 0
    }
    
    var width: CGFloat {
        return UIScreen.width - 2 * 40
    }
    
    func addLabel(_ item: RNDElaboraField?) {
        guard let item = item,
            let key = item.key else {
            return
        }
        let label = self.label
        stackView.addArrangedSubview(label)
        label.snp.makeConstraints{
            $0.width.equalTo(width * item.koef/summKoef).priority(999)
            $0.height.equalTo(stackView)
        }
        handlerLabel?(label, key)
    }
    
    func addLine() {
        let view = RNDMeanLine()
        stackView.addArrangedSubview(view)
        view.snp.makeConstraints{
            $0.width.equalTo(1).priority(999)
            $0.height.equalTo(stackView)
        }
    }

    func prepareFields(_ fields: [RNDElaboraField]?, handlerLabel: AliasFieldsBlock?) {
        self.fields = fields
        self.handlerLabel = handlerLabel
        addLabel(fields?.first)
        fields?.enumerateNonFirst {
            self.addLine()
            self.addLabel($0)
        }
    }
}
