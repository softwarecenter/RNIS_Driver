//
//  RNDElaboraTableCell.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 19.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDElaboraTableCell: RNSBaseTableCell {
    var fields: [RNDElaboraField]?
    var items: RNDCompanyData?
    
    func prepareItems(_ items: RNDCompanyData?, fields: [RNDElaboraField]?) {
        self.items = items
        self.fields = fields
    }
}
