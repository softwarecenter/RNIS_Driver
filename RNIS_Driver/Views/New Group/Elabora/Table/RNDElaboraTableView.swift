//
//  RNDElaboraTableView.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 19.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDElaboraTableView: BaseViewWithXIBInit {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    let bottom: CGFloat = 89
    @IBOutlet weak var containerTable: UIView!
    var titleView: RNDTitlesElaboraView?
    
    var item: RNDElaboraTableData? {
        didSet {
            reloadData()
        }
    }
    
    var fields: [RNDElaboraField]? {
        return item?.fields
    }
    
    var items: [RNDCompanyData]? {
        return item?.items
    }
    
    var itemsCount: Int {
        return items?.count ?? 0
    }
    
    func reloadData() {
        tableView.reloadData()
        prepareHeight()
        titleView?.fields = fields
    }
    
    func prepareHeight() {
        heightConstraint.constant = 70 * CGFloat(itemsCount) + bottom
    }
    
    override func loadNibView() {
        super.loadNibView()
        
        prepareTitle()
        containerTable.prepareShadow()
    }
    
    func prepareTitle() {
        let titleView = RNDTitlesElaboraView()
        titleView.frame = CGRect(x: 0, y: 0, width: frame.width, height: heightHeaderElabora)
        tableView.tableHeaderView = titleView
        self.titleView = titleView
    }
}
