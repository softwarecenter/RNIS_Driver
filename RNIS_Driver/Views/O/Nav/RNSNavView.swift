//
//  RNSNavView.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 29.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit
import SnapKit

@IBDesignable class RNSNavView: BaseViewWithXIBInit {
    
    @IBOutlet weak var label: UILabel!
    @IBInspectable var titleText: String? {
        didSet {
            label.text = titleText
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        prepareConstraint()
    }
    
    func prepareConstraint() {
        guard let view = superview else {
            return
        }
        snp.makeConstraints {
            $0.top.right.left.equalTo(view)
            $0.height.equalTo(heightNav)
        }
    }
}
