//
//  RNDPathNav.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 15.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDPathNav: BaseViewWithXIBInit {
    @IBOutlet weak var stackView: UIStackView!
    
    override func loadNibView() {
        super.loadNibView()
        
        reloadData()
    }
    
    func reloadData() {
        stackView.removeArrangedSubviews()
        guard let items = STRouter.viewControllers else {
            return
        }
        addButton(items.first)
        items.enumerateNonFirst { [weak self] in
            self?.addSpace()
            self?.addButton($0)
        }
    }
    
    func addButton(_ vc: UIViewController?) {
        guard let vc = vc,
            let title = vc.title else {
            return
        }
        let view = UIButton()
        view.backgroundColor = .clear
        let font = UIFont.robotoRegular14
        view.titleLabel?.font = font
        view.setTitle(title, for: .normal)
        view.setTitleColor(.BDBDBD, for: .normal)
        let width = title.width(font) + 15
        view.touchUpInside {
            STRouter.popTo(vc)
        }
        addView(view, width: width)
    }
    
    func addSpace() {
        let image = #imageLiteral(resourceName: "next_point")
        let view = UIImageView(image: image)
        view.contentMode = .center
        addView(view, width: image.size.width)
    }
    
    func addView(_ view: UIView, width: CGFloat) {
        view.snp.makeConstraints {
            $0.width.equalTo(width)
        }
        stackView.addArrangedSubview(view)
    }
    
    deinit {
        print("RNDPathNav deinit")
    }
}
