//
//  RNSGrayLineView.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 28.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNSGrayLineView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .F2F2F2
    }

}
