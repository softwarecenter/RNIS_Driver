//
//  UIViewController+Storyboards.swift
//  Spytricks
//
//  Created by Артем Кулагин on 13.07.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import Foundation

extension STLurkView {
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        if touchDisabled {
            return true
        }
        return pointAviable(point)
    }
    
    func pointAviable(_ point: CGPoint) -> Bool {
        for item in rects ?? [] {
            if item?.rect?.contains(point) ?? false {
                return false
            }
        }
        return true
    }
}
