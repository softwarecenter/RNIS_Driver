//
//  STLurkView.swift
//  Spytricks
//
//  Created by Артем Кулагин on 10.07.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import UIKit

class STLurkView: UIView {
    
    var graphLayer: CALayer?
    var rects: [STLurkRect?]? {
        didSet {
            updateLayer()
        }
    }
    
    var rectLurk: STLurkRect? {
        didSet {
            rects = [rectLurk]
        }
    }
    
    var rect: CGRect? {
        didSet {
            rects = [STLurkRect(rect)]
        }
    }
    
    var inset: Float?
    
    var touchDisabled = false
    
    convenience init(view: UIView?, rects: [STLurkRect]? = nil) {
        self.init(frame: CGRect.zero)
        if let view = view {
            view.addSubview(self)
            self.snp.makeConstraints { (make) in
                make.edges.equalTo(view)
            }
        }
        alpha = 0
        
        backgroundColor = .clear
        self.rects = rects
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateLayer()
    }
    
    func prepereTopExit(_ inset: Float) {
        self.inset = inset
    }
}
