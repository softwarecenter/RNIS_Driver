//
//  STLurkView+Bezier.swift
//  Spytricks
//
//  Created by Артем Кулагин on 13.07.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import Foundation

extension STLurkView {
    
    var path: UIBezierPath {
        let rect = self.bounds
        let path = UIBezierPath(rect: rect)
        for item in rects ?? [] {
            guard let item = item, let rect = item.rect else {
                continue
            }
            let pathRect = UIBezierPath(roundedRect: rect, cornerRadius: item.cornerRadius ?? 7).reversing()
            path.append(pathRect)
        }
        return path
    }
    
    func updateLayer() {
        self.graphLayer?.removeFromSuperlayer()
        let layer = CAShapeLayer()
        layer.path = path.cgPath
        layer.fillColor = UIColor.blackAlpha50.cgColor
        self.layer.addSublayer(layer)
        graphLayer = layer
    }
}
