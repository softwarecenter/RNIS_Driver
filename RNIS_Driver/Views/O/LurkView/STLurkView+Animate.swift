//
//  STLurkView+Animate.swift
//  Spytricks
//
//  Created by Артем Кулагин on 12.07.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import Foundation

extension STLurkView {
    
    func showAnimate(_ complete: EmptyBlock? = nil){
        animateAlpha(alpha: 1, complete)
    }
    
    func halfHideAnimate(_ complete: EmptyBlock? = nil){
        animateAlpha(alpha: 0.5, complete)
    }
    
    func animateAlpha(alpha: CGFloat, _ complete: EmptyBlock?) {
        animate({
            self.alpha = alpha
        }, complete)
    }
    
    func animate(_ animations: @escaping EmptyBlock, _ complete: EmptyBlock?) {
        UIView.animate(withDuration: 0.3, animations: animations) { _ in
            complete?()
        }
    }
    
    func show() {
        alpha = 1
    }
    
    func hide() {
        alpha = 0
    }
}
