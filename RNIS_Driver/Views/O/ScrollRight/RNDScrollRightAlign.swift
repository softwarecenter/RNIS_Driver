//
//  RNDScrollView.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 12.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

class RNDScrollRightAlign: UIScrollView {
    
    var firstLoad = true
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if firstLoad {
            firstLoad = false
            let offsetX = contentSize.width - frame.width
            if offsetX > 0 {
                setContentOffset(CGPoint(x: offsetX, y: 0), animated: false)
            }
        }
    }
}
