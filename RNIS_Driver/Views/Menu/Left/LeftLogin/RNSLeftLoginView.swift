//
//  RNSLeftLoginView.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 28.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNSLeftLoginView: BaseViewWithXIBInit {

    @IBAction func actionLogOut(_ sender: Any) {
        STRouter.showLoginPullAnimation()
    }
}
