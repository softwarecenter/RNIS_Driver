//
//  RNDCalendar+Action.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 29.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDCalendar {
    
    func prepareHandlers() {
        diapazonButton.handlerAction = { [weak self] view in
            self?.diapazonAction(view)
        }
        
        date1Button.handlerAction = { [weak self] view in
            self?.date1Action(view)
        }
        
        date2Button.handlerAction = { [weak self] view in
            self?.date2Action(view)
        }
        
        overallButton.handlerAction = { [weak self] view in
            self?.overallAction(view)
        }
    }
    
    func overallAction(_ view: UIView?) {
        let vc = RNDPickerController.initialControllerType()
        vc?.startDate = date2
        vc?.type = type
        vc?.handlerData = { [weak self] item in
            self?.date1 = item?.date1
            self?.date2 = item?.date2
            self?.updateOverallButton()
        }
        vc?.modalPresentationStyle = .popover
        vc?.preferredContentSize = sizePopover
        STRouter.presentAsPopover(vc, view: view)
    }
    
    func diapazonAction(_ view: UIView?) {
        let vc  = RNDAlertDiapazon.controller {[weak self] type in
            self?.prepareType(type)
        }
        STRouter.presentAsPopover(vc, view: view)
    }
    
    func date1Action(_ view: UIView?) {
        moveNowToSelectType()
        let vc = dateAction(view, date: date1) { [weak self] date in
            self?.date1 = date
            self?.updateDateSelectLabels()
        }
        vc?.beyondDate = date2
        STRouter.presentAsPopover(vc, view: view)
    }
    
    func date2Action(_ view: UIView?) {
        moveNowToSelectType()
        let vc = dateAction(view, date: date2) { [weak self] date in
            self?.date2 = date
            self?.updateDateSelectLabels()
        }
        vc?.beforeDate = date1
        STRouter.presentAsPopover(vc, view: view)
    }
    
    func dateAction(_ view: UIView?, date: Date?, handlerSelect: AliasDateBlock?) -> RNDAlertCalendarDate? {
        let vc = RNDAlertCalendarDate.initialControllerType()
        vc?.startDate = date
        vc?.handlerSelect = handlerSelect
        vc?.modalPresentationStyle = .popover
        vc?.preferredContentSize = sizePopover
        return vc
    }
}
