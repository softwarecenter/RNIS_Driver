//
//  RNDCalendar+Type.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 29.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDCalendar {
    
    func prepareType() {
        switch type {
        case .select:
            prepareSelectType()
        case .now:
            prepareNowType()
        default:
            prepareOveralType()
        }
        updateDiapazonButton()
        updateDateLabels()
    }
    
    func prepareType(_ type: DiapazonType) {
        self.type = type
        prepareType()
    }
    
    func prepareSelectType() {
        showDate1Date2Buttons()
        updateDateSelectLabels()
    }
    
    func prepareOveralType() {
        updateDatesAtEdge()
        visibleDate1Date2Buttons(false)
        updateOverallButton()
    }
    
    func prepareNowType() {
        let current = Date()
        date1 = current.startOfYear
        date2 = current
        prepareSelectType()
    }
    
    func moveNowToSelectType() {
        if type == .now {
            type = .select
            updateDiapazonButton()
        }
    }
}
