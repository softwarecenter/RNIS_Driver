//
//  RNDCalendar+Action.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 08.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDCalendar {
    @IBAction func formAction(_ sender: Any) {
        handlerForm?()
    }
}
