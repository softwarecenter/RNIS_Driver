//
//  RNDCalendar.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 29.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNDCalendar: BaseViewWithXIBInit {
    
    @IBOutlet var betweenConstraint: NSLayoutConstraint!
    
    var type: DiapazonType = .month
    var handlerForm: EmptyBlock?
    
    @IBOutlet weak var date1Label: UILabel!
    @IBOutlet weak var date2Label: UILabel!
    
    @IBOutlet weak var diapazonButton: RNSCalendarButton!
    @IBOutlet weak var date1Button: RNSCalendarButton!
    @IBOutlet weak var date2Button: RNSCalendarButton!
    
    @IBOutlet weak var overallButton: RNSCalendarButton!

    var date1 = Date.currentMinusMonth?.startOfMonth
    var date2 = Date.currentMinusMonth?.endOfMonth
    let sizePopover: CGSize = CGSize(width: 280, height: 344)
    
    var item: RNDCalendarData? {
        set {
            guard let newValue = newValue else {
                return
            }
            date1 = newValue.date1
            date2 = newValue.date2
            type = newValue.type
            prepareType()
        }
        get {
            return RNDCalendarData(type, date1: date1, date2: date2)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        prepareConstraint()
        prepareHandlers()
        prepareType()
    }
    
    func prepareConstraint() {
        guard let view = superview else {
            return
        }
        snp.makeConstraints {
            $0.top.equalTo(view).inset(heightNav)
            $0.right.left.equalTo(view)
            $0.height.equalTo(69)
        }
    }
}
