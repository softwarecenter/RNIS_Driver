//
//  RNDCalendar+UI.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 05.12.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import Foundation

extension RNDCalendar {
    
    func showDate1Date2Buttons() {
        visibleDate1Date2Buttons(true)
    }
    
    func visibleDate1Date2Buttons(_ show: Bool) {
        UIView.animateConstrains(self) {
            self.betweenConstraint.isActive = !show
            self.date1Button.isHidden = !show
            self.date2Button.isHidden = !show
            self.overallButton.isHidden = show
        }
    }
    
    func updateDiapazonButton() {
        diapazonButton.text = type.rawValue
    }
    
    func updateDateSelectLabels() {
        date1Button.text = date1?.stringDDMMyyyy ?? "От"
        date2Button.text = date2?.stringDDMMyyyy ?? "До"
        updateDateLabels()
    }
    
    func updateOverallButton() {
        overallButton.text = type.string(date2)
        updateDateLabels()
    }
    
    func updateDatesAtEdge() {
        let item = RNDEdgeDate(date2, type: type)
        date1 = item.date1
        date2 = item.date2
    }
    
    func updateDateLabels() {
        date1Label.text = date1?.stringDDMMyyyy
        date2Label.text = date2?.stringDDMMyyyy
    }
}

