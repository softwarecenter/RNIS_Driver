//
//  RNSCalendarButton.swift
//  RNIS_Driver
//
//  Created by Артем Кулагин on 29.11.2017.
//  Copyright © 2017 SoftwareCenter. All rights reserved.
//

import UIKit

class RNSCalendarButton: BaseViewWithXIBInit {
    
    @IBOutlet weak var widthButton: NSLayoutConstraint!
    @IBOutlet weak var label: UILabel!
    var handlerAction: AliasViewBlock?
    var text: String? {
        didSet {
            label.text = text
            updateWidth()
        }
    }
    
    var touch: EmptyBlock?
    
    func updateWidth() {
        widthButton.constant = label.widthText + 45
    }
    
    @IBAction func actionBtn(_ sender: Any) {
        handlerAction?(self)
    }
}
